plugins {
    kotlin("jvm") version "1.9.20-Beta2"
    id("com.github.weave-mc.weave-gradle") version "649dba7468"
}

group = "dev.chloe"
version = "1.0-SNAPSHOT"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    implementation("org.ow2.asm:asm:9.4")
    implementation("org.ow2.asm:asm-tree:9.4")
    implementation("org.ow2.asm:asm-commons:9.4")
    implementation("org.ow2.asm:asm-util:9.4")
}

kotlin {
    jvmToolchain(8)
}

val agent by tasks.registering(Jar::class) {
    archiveAppendix.set("Agent")
    group = "build"

    from(sourceSets.main.get().output)
    from({ configurations.runtimeClasspath.get().map { zipTree(it) } }) {
        exclude("**/module-info.class")
    }

    manifest.attributes(
        "Premain-Class" to "tech.chloehook.agent.JavaAgentKt",
        "Agent-Class" to "tech.chloehook.agent.JavaAgentKt",
        "Can-Retransform-Classes" to "true",
        "Can-Redefine-Classes" to "true"
     )
}

tasks.build {
    dependsOn(agent)
}