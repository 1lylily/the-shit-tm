package tech.chloehook.hooks.impl

import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.util.AxisAlignedBB
import org.objectweb.asm.tree.ClassNode
import tech.chloehook.event.impl.PostPositionEvent
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.hooks.api.*
import tech.chloehook.hooks.api.IHook
import tech.chloehook.main.ChloeHook
import kotlin.properties.Delegates

/**
 * Hooks into the EntityPlayerSP class.
 */
internal class EntityPlayerSPHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the sendChatMessage method.
            method("sendChatMessage") {
                // Hook at the start of the method.
                head {
                    // Create a new ChatSendEvent.
                    newInstance("tech/chloehook/event/impl/ChatSendEvent", "(Ljava/lang/String;)V") {
                        // Get the message being sent.
                        aload[1]
                    }
                    // Duplicate the event reference.
                    dup
                    // Publish the event.
                    publishEvent()
                    // Check whether the event is cancelled.
                    invokeVirtual(
                        "tech/chloehook/event/impl/ChatSendEvent",
                        "getCancelled",
                        "()Z"
                    )
                    // If the event is cancelled,
                    ifEq {
                        // return.
                        vReturn
                    }
                    // Frames are a bitch.
                    fSame
                }
            }
            // Find the onUpdate method.
            method("onUpdate") {
                // Hook at the start of the method.
                head {
                    // Create a new UpdateEvent.
                    newInstance("tech/chloehook/event/impl/UpdateEvent")
                    // Publish the event.
                    publishEvent()
                }
            }
            // Find the onUpdateWalkingPlayer method.
            method("onUpdateWalkingPlayer") {
                // Hook at the start of the method.
                head {
                    // Get a reference to the current object.
                    aload[0]
                    // Call the method for pre-position updates, as it is too complex to implement in bytecode.
                    invokeStatic(
                        "tech/chloehook/hooks/impl/EntityPlayerSPHookKt",
                        "positionPre",
                        "(Lnet/minecraft/client/entity/EntityPlayerSP;)V"
                    )
                }
                // Hook at the end of the method.
                tail {
                    // Get a reference to the current object.
                    aload[0]
                    // Call the method for post-position updates, as it is too complex to implement in bytecode.
                    invokeStatic(
                        "tech/chloehook/hooks/impl/EntityPlayerSPHookKt",
                        "positionPost",
                        "(Lnet/minecraft/client/entity/EntityPlayerSP;)V"
                    )
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/entity/EntityPlayerSP"

}

private var x by Delegates.notNull<Double>()
private lateinit var bb: AxisAlignedBB
private var z by Delegates.notNull<Double>()
private var yaw by Delegates.notNull<Float>()
private var pitch by Delegates.notNull<Float>()
private var onGround by Delegates.notNull<Boolean>()

@Suppress("Unused")
fun positionPre(player: EntityPlayerSP) {
    x = player.posX
    bb = player.entityBoundingBox
    z = player.posZ
    yaw = player.rotationYaw
    pitch = player.rotationPitch
    onGround = player.onGround
    val event = PrePositionEvent(x, bb.minY, z, yaw, pitch, onGround)
    ChloeHook.eventBus.publish(event)
    player.posX = event.x
    player.entityBoundingBox = player.entityBoundingBox.offset(0.0, bb.minY - event.y, 0.0)
    player.posZ = event.z
    player.rotationYaw = event.yaw
    player.rotationPitch = event.pitch
    player.onGround = event.onGround
}

@Suppress("Unused", "LocalVariableName")
fun positionPost(player: EntityPlayerSP) {
    val temp_x = player.posX
    val temp_bb = player.entityBoundingBox
    val temp_z = player.posZ
    val temp_yaw = player.rotationYaw
    val temp_pitch = player.rotationPitch
    val temp_onGround = player.onGround
    player.posX = x
    player.entityBoundingBox = bb
    player.posZ = z
    player.rotationYaw = yaw
    player.rotationPitch = pitch
    player.onGround = onGround
    ChloeHook.eventBus.publish(PostPositionEvent(temp_x, temp_bb.minY, temp_z, temp_yaw, temp_pitch, temp_onGround))
}