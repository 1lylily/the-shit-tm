package tech.chloehook.hooks.impl

import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.api.*
import tech.chloehook.hooks.api.IHook

/**
 * Hooks into the Minecraft class.
 */
internal class MinecraftHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the startGame method.
            method("startGame") {
                // Hook at the start of the method.
                head {
                    // Create a new StartGameEvent.
                    newInstance("tech/chloehook/event/impl/StartGameEvent")
                    // Publish the event.
                    publishEvent()
                    // Print a debug message.
                    println("[ChloeHook Debug] Hooked startup.")
                    outPrintln("[ChloeHook Debug] Startup.")
                }
            }
            // Find the runTick method.
            method("runTick") {
                // Hook at the start of the method.
                head {
                    // Create a new TickEvent.
                    newInstance("tech/chloehook/event/impl/TickEvent")
                    // Publish the event.
                    publishEvent()
                }
                // Find the dispatchKeypresses invoke.
                invoke("dispatchKeypresses") {
                    // Create a new KeyPressEvent.
                    newInstance("tech/chloehook/event/impl/KeyPressEvent")
                    // Publish the event.
                    publishEvent()
                }
            }
            /*
            // Find the session field.
            field("session") {
                // Make it mutable
                mutable()
            }

             */
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/Minecraft"

}