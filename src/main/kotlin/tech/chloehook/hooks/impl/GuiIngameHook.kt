package tech.chloehook.hooks.impl

import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.api.*
import tech.chloehook.hooks.api.Shift.*
import tech.chloehook.hooks.api.IHook

/**
 * Hooks into the GuiIngame class.
 */
internal class GuiIngameHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the renderGameOverlay method.
            method("renderGameOverlay") {
                // Inject at the head of the method.
                head {
                    // Create a new Render2DEvent.
                    newInstance("tech/chloehook/event/impl/Render2DEvent")//"(Lnet/minecraft/client/gui/ScaledResolution;)V") {
                    // Publish the event.
                    publishEvent()
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/gui/GuiIngame"

}