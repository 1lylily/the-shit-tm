package tech.chloehook.hooks.impl

import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.api.*
import tech.chloehook.hooks.api.Shift.*
import tech.chloehook.hooks.api.IHook

/**
 * Hooks into the EntityRenderer class.
 */
internal class EntityRendererHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the renderWorldPass method.
            method("renderWorldPass") {
                // Find the "hand" ldc.
                ldc("hand") {
                    // Create a new Render3DEvent.
                    newInstance("tech/chloehook/event/impl/Render3DEvent")
                    // Publish the event.
                    publishEvent()
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/renderer/EntityRenderer"

}