package tech.chloehook.hooks.impl

import org.lwjgl.input.Keyboard
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.InsnNode
import tech.chloehook.hooks.api.IHook
import tech.chloehook.hooks.api.asm
import tech.chloehook.hooks.api.head
import tech.chloehook.hooks.api.method

/**
 * Hooks into the GuiChat class.
 */
internal class GuiChatHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the keyTyped method.
            method("keyTyped") {
                // Hook at the beginning of the method.
                head {
                    // Load the key code.
                    iload[2]
                    // Load the key code for TAB.
                    ldcInt[Keyboard.KEY_TAB]
                    // If the key code is TAB, continue.
                    ifIntCmpNe {
                        // Create a new RenderGameOverlayEvent.
                        newInstance("tech/chloehook/event/impl/ChatCompleteEvent")
                        // Duplicate the event reference.
                        dup
                        // Publish the event.
                        publishEvent()
                        // Check whether the event is cancelled.
                        invokeVirtual(
                            "tech/chloehook/event/impl/ChatCompleteEvent",
                            "getCancelled",
                            "()Z"
                        )
                        // If the event is cancelled,
                        ifEq {
                            // return. There is no need to continue.
                            vReturn
                        }
                    }
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/gui/GuiChat"

    override val computeFrames: Boolean = true

}