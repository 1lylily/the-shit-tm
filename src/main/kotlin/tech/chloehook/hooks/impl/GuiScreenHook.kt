package tech.chloehook.hooks.impl

import org.lwjgl.input.Keyboard
import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.api.IHook
import tech.chloehook.hooks.api.asm
import tech.chloehook.hooks.api.head
import tech.chloehook.hooks.api.method

/**
 * Hooks into the GuiScreen class.
 */
internal class GuiScreenHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the keyTyped method.
            method("keyTyped") {
                // Inject at the head of the method.
                head {
                    outPrintln("KeyPressed AAAAA")
                    // Load the key code.
                    iload[2]
                    // Load the key code for F4.
                    ldcInt[Keyboard.KEY_F4]
                    // If the key code is F4, continue.
                    ifIntCmpNe {
                        // Get the current screen instance.
                        aload[0]
                        // Open the alt manager.
                        invokeStatic(
                            "tech/chloehook/alt/api/GuiAltManagerKt",
                            "open",
                            "(Lnet/minecraft/client/gui/GuiScreen;)V"
                        )
                    }
                    // Frames are a bitch.
                    fSame
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/client/gui/GuiScreen"

}