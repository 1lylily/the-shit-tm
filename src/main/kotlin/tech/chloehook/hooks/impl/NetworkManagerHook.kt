package tech.chloehook.hooks.impl

import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.api.*
import tech.chloehook.hooks.api.Shift.*
import tech.chloehook.hooks.api.IHook

/**
 * Hooks into the NetworkManager class.
 */
internal class NetworkManagerHook : IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    override fun transform(cn: ClassNode) =
        cn.asm {
            // Find the channelRead0 method.
            method("channelRead0") {
                // Hook at the beginning of the method.
                head {
                    // Create a new PacketReceiveEvent.
                    newInstance("tech/chloehook/event/impl/PacketReceiveEvent", "(Lnet/minecraft/network/Packet;)V") {
                        // Get the packet from the parameters.
                        aload[2]
                    }
                    // Duplicate the event reference.
                    dup
                    // Publish the event.
                    publishEvent()
                    // Check whether the event is cancelled.
                    invokeVirtual(
                        "tech/chloehook/event/impl/PacketReceiveEvent",
                        "getCancelled",
                        "()Z"
                    )
                    // If the event is cancelled,
                    ifEq {
                        // return. There is no need to continue.
                        vReturn
                    }
                    // Frames are a bitch.
                    fSame
                }
            }
            // Find the sendPacket method, specify descriptor for the single-argument one.
            method("sendPacket(Lnet/minecraft/network/Packet;)V") {
                // Hook at the beginning of the method.
                head {
                    // Create a new PacketSendEvent.
                    newInstance("tech/chloehook/event/impl/PacketSendEvent", "(Lnet/minecraft/network/Packet;)V") {
                        // Get the packet from the parameters.
                        aload[1]
                    }
                    // Duplicate the event reference.
                    dup
                    // Publish the event.
                    publishEvent()
                    // Check whether the event is cancelled.
                    invokeVirtual(
                        "tech/chloehook/event/impl/PacketSendEvent",
                        "getCancelled",
                        "()Z"
                    )
                    // If the event is cancelled,
                    ifEq {
                        // return. There is no need to continue.
                        vReturn
                    }
                    // Frames are a bitch.
                    fSame
                }
            }
        }

    /**
     * The internal name of the target class.
     */
    override val target: String = "net/minecraft/network/NetworkManager"

    override val computeFrames: Boolean = true

}