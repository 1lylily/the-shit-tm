package tech.chloehook.hooks.api

import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.*

/**
 * The beginning of the ASM DSL extension.
 */
fun ClassNode.asm(func: ClassNode.() -> Unit): Unit = this.func()

/**
 * Gets a method node by name.
 */
fun ClassNode.method(name: String, func: MethodNode.() -> Unit): Unit =
    this.methods.find { (it.name+it.desc).startsWith(name) }?.func()
    ?: throw NullPointerException("Method not found.")

fun ClassNode.field(name: String, func: FieldNode.() -> Unit): Unit =
    this.fields.find { (it.name+it.desc).startsWith(name) }?.func()
        ?: throw NullPointerException("Field not found.")

fun FieldNode.mutable() {
    this.access -= ACC_FINAL
}

/**
 * Adds instructions to the beginning of an instruction list.
 */
fun MethodNode.head(func: AbstractedInsnList.() -> Unit): Unit =
    this.instructions.insert(AbstractedInsnList().also(func))

/**
 * Adds instructions to the end of an instruction list.
 */
fun MethodNode.tail(func: AbstractedInsnList.() -> Unit): Unit =
    this.instructions.insert(this.instructions.findLast { it.opcode in 172..177 }?.previous ?: this.instructions.last, AbstractedInsnList().also(func))

/**
 * Adds instructions after a given invoke instruction.
 */
fun MethodNode.invoke(name: String, shift: Shift = Shift.AFTER, func: AbstractedInsnList.() -> Unit): Unit =
    this.instructions.insert(
        this.instructions.find { it is MethodInsnNode && (it.name+it.desc).startsWith(name) }?.shiftIf(shift == Shift.BEFORE)
            ?: throw NullPointerException("Invoke not found."),
        AbstractedInsnList().also(func)
    )

/**
 * Adds instructions after a given invoke instruction.
 */
fun MethodNode.ldc(cst: Any, shift: Shift = Shift.AFTER, func: AbstractedInsnList.() -> Unit): Unit =
    this.instructions.insert(
        this.instructions.find { it is LdcInsnNode && it.cst == cst }?.shiftIf(shift == Shift.BEFORE)
            ?: throw NullPointerException("Ldc not found."),
        AbstractedInsnList().also(func)
    )

private fun AbstractInsnNode.shiftIf(shift: Boolean): AbstractInsnNode = if (shift) this.previous else this

/**
 * ASM InsnList with more support for the DSL extensions.
 */
class AbstractedInsnList : InsnList() {

    /**
     * Adds the InsnNode to the InsnList.
     */
    operator fun AbstractInsnNode.unaryPlus(): Unit = this@AbstractedInsnList.add(this)

    /**
     * Gets the instance of ChloeHook into the stack.
     */
    fun getChloeHook() {
        this.add(FieldInsnNode(GETSTATIC, "tech/chloehook/main/ChloeHook", "INSTANCE", "Ltech/chloehook/main/ChloeHook;"))
    }

    /**
     * Creates a new instance of a given type. Parameters taken from stack, descriptor is ()V unless specified.
     */
    fun newInstance(target: String, desc: String = "()V", func: AbstractedInsnList.() -> Unit = {}) {
        this.add(TypeInsnNode(NEW, target))
        this.add(InsnNode(DUP))
        this.func()
        this.add(MethodInsnNode(INVOKESPECIAL, target, "<init>", desc))
    }

    /**
     * Invokes the given non-static method, parameters taken from stack.
     */
    fun invokeVirtual(owner: String, method: String, desc: String) {
        this.add(MethodInsnNode(INVOKEVIRTUAL, owner, method, desc))
    }

    /**
     * Invokes the given static method, parameters taken from stack.
     */
    fun invokeStatic(owner: String, method: String, desc: String) {
        this.add(MethodInsnNode(INVOKESTATIC, owner, method, desc))
    }

    fun ifEq(func: AbstractedInsnList.() -> Unit) {
        val label = LabelNode()
        this.add(JumpInsnNode(IFEQ, label))
        this.func()
        this.add(label)
    }

    /**
     * Jumps to the end of the IF statement if the previous 2 int stack elements are not equal.
     *
     * @param func
     * @receiver
     */
    fun ifIntCmpNe(func: AbstractedInsnList.() -> Unit) {
        val label = LabelNode()
        this.add(JumpInsnNode(IF_ICMPNE, label))
        this.func()
        this.add(label)
    }

    /**
     * Duplicates the top of the stack.
     */
    val dup: Unit
        get() = this.add(InsnNode(DUP))

    /**
     * Duplicates the top of the stack and inserts it before the 2nd element.
     */
    val dupX1: Unit
        get() = this.add(InsnNode(DUP_X1))

    /**
     * Swaps the top 2 elements of the stack.
     */
    val swap: Unit
        get() = this.add(InsnNode(SWAP))

    val pop: Unit
        get() = this.add(InsnNode(POP))

    /**
     * Return on void methods.
     */
    val vReturn: Unit
        get() = this.add(InsnNode(RETURN))

    val fSame: Unit
        get() = this.add(FrameNode(F_SAME, 0, null, 0, null))

    /**
     * Loads the given object from the parameters.
     */
    val aload: SingleArgInsnOperatorPlaceholder<Int> = SingleArgInsnOperatorPlaceholder { VarInsnNode(ALOAD, it) }

    val iload: SingleArgInsnOperatorPlaceholder<Int> = SingleArgInsnOperatorPlaceholder { VarInsnNode(ILOAD, it) }

    val istore: SingleArgInsnOperatorPlaceholder<Int> = SingleArgInsnOperatorPlaceholder { VarInsnNode(ISTORE, it) }

    /**
     * Adds the given integer constant to the stack.
     */
    val ldcInt: SingleArgInsnOperatorPlaceholder<Int> = SingleArgInsnOperatorPlaceholder {
        if (it in 0..5) {
            InsnNode(ICONST_0 + it)
        } else if (it < Byte.MAX_VALUE) {
            IntInsnNode(BIPUSH, it)
        } else {
            LdcInsnNode(it)
        }
    }

    operator fun SingleArgInsnOperatorPlaceholder<Int>.get(index: Int) {
        this@AbstractedInsnList.add(this.getter(index))
    }

    /**
     * Publishes an event in the event bus. Event bus and event reference taken from stack.
     */
    fun publishEvent() {
        this.add(MethodInsnNode(INVOKESTATIC, "tech/chloehook/event/api/PubSub", "publish", "(Ltech/chloehook/event/types/Event;)V"))
    }

    fun outPrintln(string: String) {
        this.add(FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"))
        this.add(LdcInsnNode(string))
        this.add(MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V"))
    }

}

class SingleArgInsnOperatorPlaceholder<T>(val getter: (T) -> AbstractInsnNode)

enum class Shift {
    BEFORE, AFTER
}