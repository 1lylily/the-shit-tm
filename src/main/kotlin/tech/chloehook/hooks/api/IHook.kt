package tech.chloehook.hooks.api

import org.objectweb.asm.tree.ClassNode

/**
 * An interface for ASM hooks.
 */
internal interface IHook {

    /**
     * Runs the hook's transformation on its target class.
     */
    fun transform(cn: ClassNode)

    /**
     * The internal name of the target class.
     */
    val target: String

    val computeFrames: Boolean
        get() = false

}