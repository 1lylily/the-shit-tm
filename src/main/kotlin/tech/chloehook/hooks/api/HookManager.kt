package tech.chloehook.hooks.api

import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ClassWriter.COMPUTE_FRAMES
import org.objectweb.asm.ClassWriter.COMPUTE_MAXS
import org.objectweb.asm.tree.ClassNode
import tech.chloehook.hooks.impl.*
import tech.chloehook.hooks.impl.EntityPlayerSPHook
import tech.chloehook.hooks.impl.GuiChatHook
import tech.chloehook.hooks.impl.GuiIngameHook
import tech.chloehook.hooks.impl.MinecraftHook
import java.lang.instrument.ClassFileTransformer
import java.lang.instrument.Instrumentation
import java.security.ProtectionDomain

/**
 * Manages hooks and their transformation.
 *
 * @property inst The instrumentation object, to register the hook transformer.
 */
internal class HookManager(
    private val inst: Instrumentation,
    runtime: Boolean
) {

    /**
     * A map of hooks to their target's internal name.
     */
    private val hooks: MutableMap<String, IHook>

    init {
        // Register hooks.
        println("[ChloeHook] Registering hooks...")
        this.hooks = listOf(
            MinecraftHook(),
            GuiIngameHook(),
            EntityPlayerSPHook(),
            GuiChatHook(),
            NetworkManagerHook(),
            EntityRendererHook(),
            GuiScreenHook()
        ).associateBy { it.target }.toMutableMap()
        // Register the hook transformer.
        this.inst.addTransformer(object : ClassFileTransformer {
            /**
             * Applies the transformations.
             */
            override fun transform(
                loader: ClassLoader?,
                className: String,
                classBeingRedefined: Class<*>?,
                protectionDomain: ProtectionDomain?,
                classfileBuffer: ByteArray
            ): ByteArray? {
                // Find the hook object for this class, return null if none.
                val hook = hooks[className] ?: return null
                // Visit the class with the class node.
                val cn = ClassNode()
                val reader = ClassReader(classfileBuffer)
                reader.accept(cn, 0)
                // Run the hook's transformation.
                hook.transform(cn)
                // Write the new bytes.
                val writer = ClassWriter(reader, if (hook.computeFrames) COMPUTE_FRAMES else COMPUTE_MAXS)
                cn.accept(writer)
                // Remove the hook from the hooks list, so it doesn't hook again.
                this@HookManager.hooks.remove(hook.target)
                // Write the bytes.
                return writer.toByteArray()
            }
        }, runtime)
        // If we are doing runtime injection,
        if (runtime) {
            // re-transform the classes.
            val loaded: List<String> = this.inst.allLoadedClasses.filter {
                try {
                    it.canonicalName.toString()
                    return@filter true
                } catch (_: Throwable) {}
                false
            }.map { it?.canonicalName?.replace(".", "/") ?: throw NullPointerException("Impossible motherfucking error.") }
            this.hooks.keys.filter { loaded.contains(it) }.map { Class.forName(it.replace("/", ".")) }.forEach {
                println("Retransforming ${it.name}")
                try {
                    this.inst.retransformClasses(it)
                } catch (e: Throwable) {
                    e.printStackTrace(System.out)
                }
                println("Retransformed ${it.name}")
            }
        }
    }

}