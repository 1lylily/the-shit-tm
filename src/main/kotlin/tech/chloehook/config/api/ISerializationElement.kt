package tech.chloehook.config.api

import com.google.gson.JsonObject

/**
 * Represents a serialization element, a value or group of values inside the serialization system.
 */
internal interface ISerializationElement {

    /**
     * Add the serialized value to the given JsonObject.
     */
    fun serialize(json: JsonObject)

    /**
     * Locate and read the serialized value from the given JsonObject.
     */
    fun deserialize(json: JsonObject)

}