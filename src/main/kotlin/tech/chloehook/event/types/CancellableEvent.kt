package tech.chloehook.event.types

interface CancellableEvent : Event {

    /**
     * Whether the event is cancelled. If an event is cancelled, it's action will not be performed.
     */
    var cancelled: Boolean

}