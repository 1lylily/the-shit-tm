package tech.chloehook.event.impl

import tech.chloehook.event.types.CancellableEvent

/**
 * Called when the client sends a chat message.
 */
class ChatCompleteEvent : CancellableEvent {
    override var cancelled: Boolean = false
}