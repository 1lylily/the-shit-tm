package tech.chloehook.event.impl

import tech.chloehook.event.types.Event

/**
 * Called every player tick.
 */
class PrePositionEvent(var x: Double, var y: Double, var z: Double, var yaw: Float, var pitch: Float, var onGround: Boolean) : Event

/**
 * Called every player tick.
 */
class PostPositionEvent(val x: Double, val y: Double, val z: Double, val yaw: Float, val pitch: Float, val onGround: Boolean) : Event