package tech.chloehook.event.impl

import tech.chloehook.event.types.Event

/**
 * Called when the game starts, used for the initialization of the client.
 */
public class StartGameEvent : Event