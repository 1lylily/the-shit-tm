package tech.chloehook.event.impl

import org.lwjgl.input.Keyboard
import tech.chloehook.event.types.Event

/**
 * Called when a key is pressed and no gui is open.
 *
 * @property code The code of the key being pressed.
 * @property state Whether the key is being pressed or released.
 */
public class KeyPressEvent : Event {

    public val code: Int =
        if (Keyboard.getEventKey() == 0) Keyboard.getEventCharacter().code + 256
        else Keyboard.getEventKey()

    public val state: Boolean = Keyboard.getEventKeyState()

}