package tech.chloehook.event.impl

import tech.chloehook.event.types.Event

/**
 * Called every game tick. Not player tick, it is not guaranteed that the player is in a world right now.
 */
class TickEvent : Event