package tech.chloehook.event.impl

import tech.chloehook.event.types.CancellableEvent

/**
 * Called when the client sends a chat message.
 */
public class ChatSendEvent(public val message: String) : CancellableEvent {
    override var cancelled: Boolean = false
}