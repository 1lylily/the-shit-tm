package tech.chloehook.event.impl

import net.minecraft.network.Packet
import tech.chloehook.event.types.CancellableEvent

/**
 * Fired when a packet is received.
 *
 * @property packet The packet being received.
 * @property cancelled If the event is cancelled, the packet will not be processed.
 */
class PacketReceiveEvent(val packet: Packet<*>) : CancellableEvent {
    override var cancelled: Boolean = false
}

/**
 * Fired when a packet is sent.
 *
 * @property packet The packet being sent.
 * @property cancelled If the event is cancelled, the packet will not be sent.
 */
class PacketSendEvent(val packet: Packet<*>) : CancellableEvent {
    override var cancelled: Boolean = false
}