package tech.chloehook.event.impl

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.ScaledResolution
import tech.chloehook.event.types.Event

/**
 * Called every frame, before the game overlay is rendered.
 *
 * @property sr The ScaledResolution instance for this frame.
 */
class Render2DEvent : Event {

    val sr: ScaledResolution = ScaledResolution(Minecraft.getMinecraft())

}

/**
 * Called every time the world is rendered.
 */
class Render3DEvent : Event