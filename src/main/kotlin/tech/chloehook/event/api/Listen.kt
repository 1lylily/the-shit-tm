package tech.chloehook.event.api

/**
 * Indicates that a function is an event listener.
 */
annotation class Listen
