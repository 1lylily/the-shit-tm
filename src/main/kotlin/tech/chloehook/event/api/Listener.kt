package tech.chloehook.event.api

import tech.chloehook.event.types.Event

/**
 * Internally represents a subscriber in the event bus.
 */
class Listener private constructor(
    val type: Class<*>,
    private val listener: (Event) -> Unit
) {

    /**
     * Checks if the given event is of the same type as the subscriber.
     */
    fun isOf(e: Event): Boolean { return e::class.java == type }

    /**
     * Publish an event to the subscriber, casting it to the correct type.
     */
    @Suppress("Unchecked_Cast")
    fun publish(e: Event) = listener(e)

    companion object {

        fun create(type: Class<*>, listener: (Any) -> Unit): Listener = Listener(type, listener as (Event) -> Unit)

    }

}