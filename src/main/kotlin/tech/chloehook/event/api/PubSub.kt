package tech.chloehook.event.api

import tech.chloehook.event.types.Event
import tech.chloehook.utils.putAndGet
import java.lang.reflect.Method

/**
 * ChloeHook's event bus.
 *
 * The only static class in the entire client, due to the need to access it with ASM.
 *
 * TODO: only partially nuke listener cache on subscribe/unsubscribe
 */
@Suppress("Unused")
object PubSub {

    /**
     * Holds all subscribers currently registered.
     */
    private val subscribers: MutableMap<Any, MutableMap<Class<out Event>, MutableList<Listener>>> = HashMap()

    /**
     * A cache for listeners.
     */
    private val listenerCache: MutableMap<Class<out Event>, MutableList<Listener>> = HashMap()

    /**
     * Holds a list of classes to known subscriber methods inside them.
     */
    private val subscriberCache: MutableMap<Class<*>, List<Method>> = HashMap()

    /**
     * Subscribes an object to the event bus.
     *
     * @param subscriber The object to subscribe.
     * @param cache Whether to cache the class of the object.
     */
    @Suppress("Unchecked_Cast")
    fun subscribe(subscriber: Any, cache: Boolean = true) {
        // Check the cache for the subscriber's methods.
        val methods: List<Listener> = (this.subscriberCache[subscriber::class.java]
            // If the cache does not have that subscriber, reflect its methods.
            ?: this.reflectMethods(subscriber::class.java, cache))
            // Map the methods to listeners.
                .map { method -> Listener.create(method.parameters[0].type) { method.invoke(subscriber, it) } }
        // Check if the subscriber map contains this subscriber.
        if (this.subscribers.containsKey(subscriber))
            // Return if it does, we don't want to add it twice.
            return
        // Put an empty listener map into the subscriber map.
        with (this.subscribers.putAndGet(subscriber, HashMap())) {
            // Iterate through the reflected methods.
            methods.forEach {
                // Add it to the list for the event type.
                (this[it.type] ?: this.putAndGet(it.type as Class<out Event>, ArrayList())).add(it)
            }
            // Destroy the listener cache, as it is no longer up to date.
            this@PubSub.listenerCache.clear()
        }
    }

    /**
     * Unsubscribes an object from the event bus.
     *
     * @param subscriber The object to unsubscribe.
     */
    fun unsubscribe(subscriber: Any) {
        // Remove the subscriber from the subscriber map.
        this.subscribers.remove(subscriber)
        // Destroy the listener cache, as it is no longer up to date.
        this@PubSub.listenerCache.clear()
    }

    /**
     * Publishes an event to all subscribed listeners.
     *
     * @param event The event to publish.
     */
    @JvmStatic
    fun publish(event: Event) {
        // Check the cache for the listeners for this event.
        val listeners = this.listenerCache[event::class.java]
            // If it is not present, build the cache.
            ?: this.buildListenerCache(event::class.java)
        // Iterate through the listeners.
        listeners.forEach {
            // Publish the event.
            it.publish(event)
        }
    }

    /**
     * Builds a listener cache for the given event type.
     *
     * @param clazz The Java class of the event to build for.
     * @param addToCache Whether to add the list to the [listenerCache].
     * @return A mutable list of listeners.
     */
    private fun buildListenerCache(clazz: Class<out Event>, addToCache: Boolean = true): MutableList<Listener> =
        ArrayList<Listener>().also { list ->
            // Iterate through the subscriber list.
            this.subscribers.values.forEach {
                // Add listeners of the given type to the listener list.
                list.addAll(it[clazz] ?: return@forEach)
            }
        }.also {
            // If addToCache is true,
            if (addToCache) {
                // add it to the cache.
                this.listenerCache[clazz] = it
            }
        }

    /**
     * Reflect the listener methods of a class.
     *
     * @param clazz The Java class to reflect.
     * @param addToCache Whether to add the methods to the [subscriberCache].
     * @return An immutable list of methods.
     */
    private fun reflectMethods(clazz: Class<*>, addToCache: Boolean): List<Method> =
        clazz.declaredMethods.filter {
            // Check for the @Listen annotation.
            if (!it.isAnnotationPresent(Listen::class.java))
                // If it is not present, return.
                return@filter false
            // Check if the parameter count is correct.
            if (it.parameterCount != 1)
                // If it is not, return.
                return@filter false
            // Check if the event parameter is an Event type.
            if (it.parameters[0].type.isAssignableFrom(Event::class.java)) {
                // If it is not, return.
                return@filter false
            }
            // Check if the method is accessible.
            @Suppress("Deprecation")
            if (!it.isAccessible) it.isAccessible = true
            // Returns true, as this is a valid method.
            return@filter true
        }.also {
            // If addToCache is true,
            if (addToCache) {
                // add the methods to the cache.
                this.subscriberCache[clazz] = it
            }
        }

}