package tech.chloehook.module.impl.player

import net.minecraft.item.ItemBlock
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.TickEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * This module reduces the delay between item uses.
 */
class FastPlace(dependency: ModuleDependency) : Module(dependency,
    "Fast Place",
    ModuleCategory.PLAYER,
    "Lowers the delay between item uses."
) {
    override var suffix: () -> String? = { this.delay.toString() }

    /**
     * Whether the delay should only be lowered when holding blocks
     */
    private var onlyBlocks by boolean("Only Blocks", true, "Only when holding blocks.")

    /**
     * The new delay between item uses. The vanilla delay is 5.
     */
    private var delay by int("Delay", 2, 0..5, "The new delay.")

    /**
     * Whether to increase the **average** delay by 0.5.
     *
     * TODO: Get better randomization.
     */
    private var randomizeDelay by boolean("Randomize Delay", true, "Randomizes the delay a bit.")

    /**
     * Run this on update.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onTick(e: TickEvent) {
        // If Only Blocks is enabled and blocks are not being held, return.
        if (this.onlyBlocks && mc.thePlayer?.heldItem?.item !is ItemBlock) return
        // If the current delay is more than Delay,
        if (mc.rightClickDelayTimer > this.delay) {
            // set it to Delay.  Randomize if needed.
            mc.rightClickDelayTimer = (this.delay + (if (randomizeDelay) Math.random() else 0.0)).toInt()
        }
    }

}