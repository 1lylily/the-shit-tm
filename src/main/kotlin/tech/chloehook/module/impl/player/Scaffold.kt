package tech.chloehook.module.impl.player

import net.minecraft.item.ItemBlock
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement
import net.minecraft.network.play.client.C0APacketAnimation
import net.minecraft.util.BlockPos
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumFacing.*
import net.minecraft.util.Vec3
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PacketSendEvent
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.*
import java.util.LinkedList
import java.util.Random
import kotlin.math.abs
import kotlin.math.max
import kotlin.properties.Delegates

private typealias BlockData = Pair<BlockPos, EnumFacing>

/**
 * Places blocks under the player.
 */
class Scaffold(dependency: ModuleDependency) : Module(dependency,
    "Scaffold",
    ModuleCategory.PLAYER,
    "Bridges for you."
) {
    override var suffix: () -> String? = { this.depth.toString() }

    private var depth by int("Depth", 2, 1..5, "The depth to search for possible block placements.")

    private var verus by boolean("Verus", false, "Fixes 14E flags on VerusAC.")

    private val queued: LinkedList<BlockData> = LinkedList()

    private var cooldown: Int = 0

    private var rotated: Boolean = false
    private var target: BlockData? = null
    private var rotations: Rotations? = null

    var lastPlace: Int = 0

    override fun onEnable() {
        this.queued.clear()
        this.cooldown = 2
        this.rotated = false
        this.target = null
        this.lastPlace = 10000
    }

    @Suppress("Unused")
    fun onPacketSend(e: PacketSendEvent) {
        // If Verus 14E fix is on, and the packet is a C08,
        if (e.packet is C08PacketPlayerBlockPlacement && this.verus) {
            // clamp the values.
            e.packet.facingX = e.packet.facingX.coerceIn(-1F, 1F)
            e.packet.facingY = e.packet.facingY.coerceIn(-1F, 1F)
            e.packet.facingZ = e.packet.facingZ.coerceIn(-1F, 1F)
        }
    }

    /**
     * Run this on update, since we need to make sure the player is in a world.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // Increment the ticks since last place counter.
        this.lastPlace++
        // If not holding blocks, return.
        if (mc.thePlayer.heldItem?.item !is ItemBlock) return
        // Decrement the cooldown.
        cooldown--
        // Get the current player position.
        val playerPos = mc.thePlayer.positionVector
        // Iterate through the queued placements.
        this.queued.toList().forEach {
            // If it is now too far away, remove it.
            if (it.first.distanceSq(playerPos) > 4.5) this.queued.remove(it)
            // If it is not air anymore, remove it.
            if (!mc.theWorld.isAirBlock(it.first)) this.queued.remove(it)
        }
        // Get the player origin.
        val playerOrigin = BlockPos(playerPos.xCoord.floor(), playerPos.yCoord.floor() - 1, playerPos.zCoord.floor())
        // If the origin is already placed, return.
        if (!mc.theWorld.isAirBlock(playerOrigin)) return
        // If the list doesn't include the origin,
        if (!this.queued.any { it.first == playerOrigin }) {
            // clear it
            this.queued.clear()
            // and recalculate.
            this.calculatePlacements(playerOrigin)
        }
        // If the list is not empty, continue.
        if (this.queued.isNotEmpty()) {
            // Check if the correct block is being targeted.
            if (this.target?.first == this.queued.first.first && this.target?.second == this.queued.first.second) {
                // If the target is correct, check if the player has rotated.
                if (this.rotated) {
                    // Get the nearest block.
                    val (pos, facing) = this.queued.pop()
                    // Place it.
                    mc.playerController.onPlayerRightClick(mc.thePlayer, mc.theWorld, mc.thePlayer.heldItem, pos.offset(facing.getOpposite()), facing, Vec3(mc.thePlayer.posX, mc.thePlayer.entityBoundingBox.minY + mc.thePlayer.eyeHeight, mc.thePlayer.posZ))
                    // Swing (silently).
                    sendPacket(C0APacketAnimation())
                    // Set the cooldown.
                    this.cooldown = 0
                    // Indicate that a block was just placed.
                    this.lastPlace = 0
                } else {
                    // If the player has not rotated, this is an edge case. Re-calculate the rotations.
                    this.rotations = (this.target as BlockData).calculateRotations(mc.thePlayer.nextTickHitOrigin)
                    // Rotate again
                    e.yaw = this.rotations!!.first
                    e.pitch = this.rotations!!.second
                    // and indicate that the player has rotated.
                    this.rotated = true
                }
            } else {
                // Set the target,
                this.target = this.queued.first
                // calculate rotations,
                this.rotations = (this.target as BlockData).calculateRotations(mc.thePlayer.nextTickHitOrigin)
                // and rotate.
                e.yaw = this.rotations!!.first
                e.pitch = this.rotations!!.second
                // Indicate that the player has rotated.
                this.rotated = true
            }
        } else {
            // Otherwise, clear the target.
            this.target = null
        }
    }

    /**
     * Populates the placement queue list.
     *
     * TODO: Optimize.
     */
    private fun calculatePlacements(origin: BlockPos) {
        // Create a list to store the solution.
        val solution: MutableList<BlockData> = ArrayList()
        // Create a map of depth to position maps.
        val layers: MutableMap<Int, MutableList<BlockData>> = HashMap()
        // Sets the origin.
        layers[0] = ArrayList(listOf(origin to UP))
        // Iterate through the depths.
        for (current in 1..depth)  {
            // Get the previous layer.
            val origins = layers[current - 1]!!
            // Create the list for this layer.
            val currentLayer: MutableList<BlockData> = ArrayList()
            // Iterate through the origins.
            origins.forEach depthShit@ { (prevOrigin, _) ->
                // Iterate through its relative blocks.
                prevOrigin.scanRelative().forEach { relative ->
                    // Check if it is an air block.
                    if (mc.theWorld.isAirBlock(relative.first)) {
                        // If it is, add it to the layer's positions.
                        currentLayer.add(relative)
                    } else {
                        // If it is not, this is the solution.
                        var currentDepth = current - 1
                        // Create a variable to hold the last iteration's position and facing.
                        var lastIter = relative
                        // Iterate through the depths.
                        while (currentDepth > 1) {
                            // Get the current depth's block from the previous depth's.
                            val currentIter = layers[currentDepth]?.find { it.first == lastIter.first.offset(lastIter.second.getOpposite()) } ?: run {
                                addChatMessage("Impossible error?!")
                                return
                            }
                            // Add this iteration to the solution.
                            solution.add(currentIter)
                            // Set the last iteration variable to the current iteration.
                            lastIter = currentIter
                            // Decrement the current depth.
                            currentDepth--
                        }
                        // Add the origin to the solution.
                        solution.add(origin to lastIter.second.getOpposite())
                        // Break from the loop.
                        return@depthShit
                    }
                }
            }
            // If the solution list is not empty, we found a solution.
            if (solution.isNotEmpty()) {
                // Break the loop.
                break
            }
            // Set the layer's positions in the layer map.
            layers[current] = currentLayer
        }
        // Put the solution into the queue.
        this.queued.addAll(solution)
    }

    /**
     * Gets the relative block positions to place from.
     */
    private fun BlockPos.scanRelative(): List<BlockData> =
        listOf(
            this.down() to DOWN,
            this.east() to EAST,
            this.west() to WEST,
            this.south() to SOUTH,
            this.north() to NORTH
        )

    /**
     * Calculate the rotations to aim at the block.
     *
     * @param hitOrigin The origin for the hit.
     */
    private fun BlockData.calculateRotations(hitOrigin: Vec3): Rotations =
        // Create a new Vec3.
        Vec3(
            // Set the target X coord to the block X coord plus
            this.first.x +
                    // If we are placing on the X axis,
                    (if (this.second.axis == Axis.X) {
                        // set it to the facing's X offset.
                        this.second.offsetX
                    } else {
                        // Otherwise, set it to a gaussian random between 0.1 and 0.9.
                        clamp(abs(gaussianRandom), 0.1, 0.9)
                    }),
            // Check whether we are placing directly below.
            (if (this.second.axis == Axis.Y) {
                // If we are, target the top of the block.
                this.first.y + 1.0
            } else {
                // Otherwise, look at the lower side of the block.
                this.first.y + 0.2
            }),
            // Set the target Z coord to the block Z coord plus
            this.first.z +
                    // If we are placing on the Z axis,
                    (if (this.second.axis == Axis.Z) {
                        // set it to the facing's Z offset.
                        this.second.offsetZ
                    } else {
                        // Otherwise, set it to a gaussian random between 0.1 and 0.9.
                        clamp(abs(gaussianRandom), 0.1, 0.9)
                    })
        )
            // Get the delta between the target position ands the hit origin.
            .delta(hitOrigin)
            // Calculate the rotations for this delta.
            .rotations

}