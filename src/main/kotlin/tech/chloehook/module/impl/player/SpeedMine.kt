package tech.chloehook.module.impl.player

import net.minecraft.item.ItemPickaxe
import net.minecraft.network.play.client.C0APacketAnimation
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.sendPacket

/**
 * This module reduces the delay between item uses.
 */
class SpeedMine(dependency: ModuleDependency) : Module(dependency,
    "Speed Mine",
    ModuleCategory.PLAYER,
    "Break blocks faster.",
) {
    override var suffix: () -> String? = { this.speed.toString() }

    /**
     * Whether to only mine fast when holding a pickaxe.
     */
    private var onlyPickaxe by boolean("Only Pickaxe", true, "Only when holding a pickaxe.")

    /**
     * The delay between breaking blocks.
     */
    private var hitDelay by int("Hit Delay", 4, 0..4, "The delay between blocks.")

    /**
     * The progress needed to break a block.
     */
    private var progress by double("Progress", 1.0, 0.0..1.0, "The progress needed to break a block.")

    /**
     * Whether to apply progress before the rest of the tick's acceleration.
     */
    private var applyProgressFirst by boolean("Apply Progress First", false, "Whether to apply progress before speeding up.")

    /**
     * The speed at which to mine blocks.
     */
    private var speed by double("Speed", 1.0, 1.0..2.0, "The speed to mine blocks at.")

    /**
     * Alternative SpeedMine technique, should be faster than normal but will flag anticheats.
     */
    private var fast by boolean("Fast", false, "Does not work with Progress First.")

    /**
     * Damage tracking, a theoretical bypass for some anticheats.
     */
    private var damageTracking by boolean("Damage Tracking", false, "Theoretical bypass.")

    private var wasBreakingLastTick = false
    private var lastTickProgress = 0.0F
    private var boostNext = 0.0F
    private var regularSpeed = 0.0F
    private var lastTracked = 0.0F

    /**
     * Run this on update.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // If holding a pickaxe or onlyPickaxe is disabled, continue.
        if (!this.onlyPickaxe || mc.thePlayer?.heldItem?.item is ItemPickaxe) {
            // If the hit delay is more than hitDelay,
            if ((mc.playerController?.blockHitDelay ?: return) > this.hitDelay) {
                // set it to hitDelay.
                mc.playerController.blockHitDelay = this.hitDelay
            }
            // If progress should be applied first,
            if (this.applyProgressFirst) {
                // and the current progress is equal to or above progress,
                if (mc.playerController.curBlockDamageMP >= this.progress) {
                    // apply progress and return.
                    mc.playerController.curBlockDamageMP = 1.0F
                    return
                }
            }
            // Calculate the damage delta.
            val damageDelta = mc.playerController.curBlockDamageMP - this.lastTickProgress
            // If a block is being broken,
            if (mc.playerController.isHittingBlock) {
                // and was last tick,
                if (this.wasBreakingLastTick) {
                    // apply speed.
                    mc.playerController.curBlockDamageMP += damageDelta * this.speed.toFloat()
                } else {
                    // Set regular breaking speed.
                    this.regularSpeed = mc.playerController.curBlockDamageMP
                    // Apply the boost
                    mc.playerController.curBlockDamageMP += this.boostNext
                    // and reset it.
                    this.boostNext = 0.0F
                    this.lastTracked = this.regularSpeed
                }
            }
            // Do damage tracking.
            if (damageTracking) {
                // While there is untracked damage,
                while (this.lastTracked + this.regularSpeed >= mc.playerController.curBlockDamageMP) {
                    // Send a swing packet.
                    sendPacket(C0APacketAnimation())
                    // Adjust last tracked damage.
                    this.lastTracked += this.regularSpeed
                }
            }
            // If progress should be applied last,
            if (!this.applyProgressFirst) {
                // and the current progress is equal to or above progress,
                if (mc.playerController.curBlockDamageMP >= this.progress) {
                    // apply progress.
                    mc.playerController.curBlockDamageMP = 1.0F
                }
            }
            // Set the fields for the next tick.
            this.wasBreakingLastTick = mc.playerController.isHittingBlock
            this.lastTickProgress = mc.playerController.curBlockDamageMP
            // Faster SpeedMine technique.
            if (this.fast && mc.playerController.curBlockDamageMP >= 1.0F) {
                // Get the target block, if it is null, return.
                val targetBlock = mc.objectMouseOver?.blockPos ?: return
                // Set the block to air.
                mc.theWorld?.setBlockToAir(targetBlock)
                // Add the boost.
                this.boostNext = damageDelta
            }
        }
    }

}