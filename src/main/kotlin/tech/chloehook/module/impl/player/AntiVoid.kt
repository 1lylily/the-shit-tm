package tech.chloehook.module.impl.player

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.ceil
import tech.chloehook.utils.isAboveVoid
import tech.chloehook.utils.up

/**
 * This module reduces the delay between item uses.
 */
class AntiVoid(dependency: ModuleDependency) : Module(dependency,
    "Anti Void",
    ModuleCategory.PLAYER,
    "Tries to save you from falling into the void."
) {
    override var suffix: () -> String? = { this.modeProperty }

    /**
     * How to try to save the player.
     */
    private var modeProperty by mode("Mode", "Test", "How to save you.", "Test")

    /**
     * Check whether the player is actually above void.
     */
    private var voidCheckProperty by boolean("Void Check", true, "Only when holding blocks.")

    /**
     * Run this on update, since we need to make sure the player is in a world.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // If the player has existed for less than 20 ticks, don't try to save.
        if (mc.thePlayer.ticksExisted < 20) return
        // If the player is on the ground, don't try to save.
        if (mc.thePlayer.onGround || mc.thePlayer.fallDistance < 2F)
        // If the void check is on, and the player is not above the void, don't try to save.
        if (voidCheckProperty && !mc.thePlayer.isAboveVoid()) return
        // Check the current mode.
        when (modeProperty) {
            // Test mode, just teleport the player up.
            "Test" -> {
                // Set the player position.
                mc.thePlayer.up(mc.thePlayer.fallDistance.ceil())
            }
        }
    }

}