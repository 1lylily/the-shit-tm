package tech.chloehook.module.impl.combat

import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.entity.EntityLivingBase
import net.minecraft.network.play.client.C02PacketUseEntity
import net.minecraft.util.Vec3
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.*
import kotlin.properties.Delegates

/**
 * Automatically rotates to and attacks targets around the player.
 */
class Aura(dependency: ModuleDependency) : Module(dependency,
    "Aura",
    ModuleCategory.COMBAT,
    "Automatically aims and attacks."
) {
    override var suffix: () -> String? = { if (this.packet) "Test" else "Legit" }

    private var targetRange by double("Target Range", 4.3, 1.0..10.0, "How far away to target players.")

    private var attackRange by double("Attack Range", 3.0, 1.0..6.0, "How far away to attack players.")

    private var autoDisable by boolean("Auto Disable", true, "Automatically disable on world change.")

    private var packet by boolean("Packet", false, "Attacks using packets instead of normally.")

    private val targets: MutableMap<Int, AuraTarget> = HashMap()

    private var target: Int? = null
    private var rotated: Boolean = false
    private var cooldown: Int = 0

    /**
     * Do the main actions of Aura on Pre.
     */
    @Listen
    @Suppress("Unused")
    fun onUpdate(e: PrePositionEvent) {
        // Decrement the cooldown.
        cooldown--
        // If we just switched worlds and Auto Disable is off, disable the module.
        if (mc.thePlayer.ticksExisted < 10 && autoDisable) {
            // Disable the module.
            this.enabled = false
            // Return.
            return
        }
        // Calculate the player hit origin.
        val hitOrigin = mc.thePlayer.hitOrigin
        // Iterate through the targets list.
        this.targets.toMap().forEach { (id, target) ->
            // Get the entity from the target.
            val entity = target.entity
            // If the entity is not present in the world, remove it.
            if (mc.theWorld.getEntityByID(id) == null) this.targets.remove(id)
            // If the entity is dead or at an invalid health, remove it.
            if (entity.isDead || entity.health <= 0) this.targets.remove(id)
            // If the entity is outside the targeting range, remove it.
            if (entity.getDistanceSqToEntity(mc.thePlayer) > this.targetRange) this.targets.remove(id)
        }
        // Iterate through loaded players.
        mc.theWorld.playerEntities.filter {
            // If the entity is invisible, ignore it.
            if (it.isInvisible) return@filter false
            // If the entity is the player, ignore it.
            if (it.entityId == mc.thePlayer.entityId) return@filter false
            // If the entity is outside the targeting range, ignore it.
            if (it.hitDistanceFrom(hitOrigin) > this.targetRange) return@filter false
            // If the entity is dead or at an invalid health, ignore it.
            if (it.isDead || it.health <= 0) return@filter false
            // The entity should be valid.
            true
        }.onEach {
            // Add them to the targets list.
            this.targets[it.entityId] = AuraTarget(it)
        }
        // Check if the targets list is empty.
        if (this.targets.isEmpty()) {
            // Set the target to null.
            this.target = null
            // Indicate that the player hasn't rotated yet, as there is nothing to rotate to.
            this.rotated = false
        } else {
            // If the current target is invalid,
            if (!this.targets.contains(this.target)) {
                // find a new one.
                this.target = this.targets.keys.random()
                // Indicate that the player hasn't rotated yet.
                this.rotated = false
                // Reset the cooldown.
                this.cooldown = 0
            }
            // Get the target.
            val (targetEntity, closestPoint, distance) = this.targets[this.target]!!.updateCache(mc.thePlayer)
            // Check if the player has rotated.
            if (this.rotated) {
                // If the target is hittable,
                if ((targetEntity.hurtTime == 0) && (cooldown <= 0) && distance <= this.attackRange) {
                    // Swing first, since this is 1.8.
                    mc.thePlayer.swingItem()
                    // Attack.
                    if (this.packet) {
                        // Attack normally, slowing you down.
                        mc.thePlayer.attackTargetEntityWithCurrentItem(targetEntity)
                    } else {
                        // Attack by packet.
                        sendPacket(C02PacketUseEntity(targetEntity, C02PacketUseEntity.Action.ATTACK))
                    }
                    // Set the cooldown.
                    cooldown = 2
                }
            }
            // TODO: improve this shit
            // Get the rotations needed.
            val (yaw, pitch) = closestPoint.delta(hitOrigin).rotations
            // Set the rotations to the needed ones.
            e.yaw = yaw
            e.pitch = pitch
            // Indicate that the player has rotated now.
            this.rotated = true
        }
    }

    /**
     * Represents an Aura target.
     *
     * @property entity The entity object.
     * @property lastCachedTick The last tick that the cache was updated.
     * @property cachedDist The cached value for distance.
     */
    class AuraTarget(
        val entity: EntityLivingBase
    ) {
        private var lastCachedTick: Int = 0
        private var cachedDist: Double = 0.0
        private var cachedClosestPoint: Vec3 by Delegates.notNull()

        /**
         * Updates the cached data.
         */
        fun updateCache(player: EntityPlayerSP): AuraTarget {
            // Set the last cached tick.
            this.lastCachedTick = player.ticksExisted
            // Get the closest point.
            this.cachedClosestPoint = this.entity.closestPoint(player.hitOrigin)
            // Re-calculate the distance.
            this.cachedDist = this.entity.hitDistanceFrom(player.hitOrigin, this.cachedClosestPoint)
            // Return the instance.
            return this
        }

        /**
         * Gets the entity as the first component.
         */
        operator fun component1(): EntityLivingBase = this.entity

        /**
         * Gets the cached closest point to the entity as the second component.
         */
        operator fun component2(): Vec3 = this.cachedClosestPoint

        /**
         * Gets the cached distance as the third component.
         */
        operator fun component3(): Double = this.cachedDist
    }

}