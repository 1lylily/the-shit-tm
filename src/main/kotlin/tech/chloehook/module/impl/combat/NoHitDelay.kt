package tech.chloehook.module.impl.combat

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * When the player jumps by holding down their jump bind, there is a 10 tick delay between jumps. This module eliminates that.
 */
class NoHitDelay(dependency: ModuleDependency) : Module(dependency,
    "No Hit Delay",
    ModuleCategory.COMBAT,
    "Removes the swing delay after missed hits."
) {

    /**
     * The best time to remove this delay is on player update, as it is called every tick.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // Set the jump ticks to 0, indicating that the delay is over.
        mc.leftClickCounter = 0
    }

}