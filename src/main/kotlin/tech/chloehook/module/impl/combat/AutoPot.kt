package tech.chloehook.module.impl.combat

import net.minecraft.item.ItemPotion
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PacketSendEvent
import tech.chloehook.event.impl.PostPositionEvent
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.addChatMessage
import tech.chloehook.utils.hasSpeedEffect
import tech.chloehook.utils.moving
import tech.chloehook.utils.sendPacket

/**
 * Helps with throwing options.
 */
class AutoPot(dependency: ModuleDependency) : Module(dependency,
    "Auto Pot",
    ModuleCategory.COMBAT,
    "Throws potions."
) {
    override var suffix: () -> String? = { this.mode }

    private var mode by mode("Mode", "Manual", "How to throw the potion.", "Manual")

    private var shouldThrow: Boolean = false
    private var throwNow: Boolean = false

    @Listen
    @Suppress("Unused")
    fun onUpdate(e: PrePositionEvent) {
        // If not on the ground, wait.
        if (!mc.thePlayer.onGround) return
        // If a packet was cancelled,
        if (this.shouldThrow) {
            // check if the player is moving.
            if (mc.thePlayer.moving) {
                // Check if the player has speed
                if (mc.thePlayer.hasSpeedEffect) {
                    // If the player is moving and has speed, rotate to pitch 2. This will hit the player during movement.
                    e.pitch = 2F
                } else {
                    // If the player doesn't have speed, rotate to pitch 36. This will hit the player during movement.
                    e.pitch = 36F
                }
            } else {
                // If the player is not moving, rotate straight up
                e.pitch = -90F
                // and jump. This will hit the player while going up
                mc.thePlayer.jump()
            }
            // and throw on post.
            this.throwNow = true
            // Indicate to not throw next tick.
            this.shouldThrow = false
        }
    }

    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onPostUpdate(e: PostPositionEvent) {
        // If rotation is done,
        if (this.throwNow) {
            // Indicate that no rotation has been done yet.
            this.throwNow = false
            // Throw the pot.
            sendPacket(C08PacketPlayerBlockPlacement(null))
        }
    }

    @Listen
    @Suppress("Unused")
    fun onPacketSend(e: PacketSendEvent) {
        // Check if the packet is a throw packet.
        if ((e.packet is C08PacketPlayerBlockPlacement
                && e.packet.stack != null
                    && e.packet.stack.item != null)
            && e.packet.stack.item is ItemPotion
            && ItemPotion.isSplash(e.packet.stack.metadata)
        ) {
            // Cancel it.
            e.cancelled = true
            // Indicate to throw pot.
            this.shouldThrow = true
            // Indicate to wait for rotation.
            this.throwNow = false
        }
    }

}