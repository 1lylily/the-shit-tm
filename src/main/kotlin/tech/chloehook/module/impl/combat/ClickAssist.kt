package tech.chloehook.module.impl.combat

import net.minecraft.util.MovingObjectPosition
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import kotlin.math.max
import kotlin.math.min

/**
 * When the player jumps by holding down their jump bind, there is a 10 tick delay between jumps. This module eliminates that.
 */
class ClickAssist(dependency: ModuleDependency) : Module(dependency,
    "Click Assist",
    ModuleCategory.COMBAT,
    "A chance to click twice."
) {

    private var chanceProperty by int("Chance", 70, 0..100, "The chance to double click.")

    /**
     * Skew of chance randomization.
     *
     * 1 - no skew/randomization
     * 0.5 - chance average is chance-(skew*0.5)
     * 1.5 - chance average is chance+(skew*0.5)
     */
    private var skewProperty by double("Chance Skew", 1.0, 0.5..1.5, "The chance average's multiplier compared to the chance.")

    /**
     * The amount to skew chance randomization.
     */
    private var skewAmountProperty by int("Skew Amount", 8, 0..20, "The amount to skew the chance average.")

    private var skewMaintainProperty by double("Skew Maintain", 0.8, 0.2..1.0, "The amount to maintain the skew average.")

    private var skewNormalizeProperty by double("Skew Normalize", 0.07, 0.0..0.5, "The amount to normalize the skew per tick.")

    private var skewNormalizeAfter by int("Normalize Ticks", 5, 1..20, "The delay before normalizing skew.")

    /**
     * Don't double-click on misses, to avoid triggering the hit cooldown.
     */
    private var notOnMissProperty by boolean("Not on Miss", true, "Don't double click a miss.")

    private var skew: Double = 1.0
    private var normalizeDelay: Int = 0

    override fun onEnable() {
        this.skew = 1.0
        this.normalizeDelay = 3
    }

    /**
     * Calculates the current chance, including skew.
     */
    private fun getSkew(): Double {
        // Get the skew amount into a constant.
        val amount = skewAmountProperty
        // If the amount is 0, skew is disabled.
        if (amount == 0) return chanceProperty.toDouble()
        // Get the multiplier value.
        val multiplier = skewProperty
        // Get the maintain value.
        val maintain = skewMaintainProperty
        TODO()
    }

    /**
     * The complete check for whether to double-click.
     */
    private fun shouldClick(): Boolean {
        // If you are in a gui, there is no point.
        if (mc.currentScreen != null) return false
        // If Not on Miss is turned on and the hit is a miss, return false.
        if (notOnMissProperty && mc.objectMouseOver?.typeOfHit != MovingObjectPosition.MovingObjectType.ENTITY) return false
        // Run the chance calculation.
        return (Math.random() * 100) <= this.getSkew()
    }

    /**
     * Ran each tick to attempt skew normalization.
     */
    private fun normalizeSkew() {
        // If the skew is already normalized, return.
        if (this.skew == this.skewProperty) return
        // If the delay is currently running,
        if (this.normalizeDelay > 0) {
            // decrement it,
            this.normalizeDelay--
            // and return.
            return
        }
        // Get a constant for the skew property.
        val targetSkew = this.skewProperty
        // Check which direction to normalize in.
        when {
            // If the current skew is bigger than the target one,
            this.skew > targetSkew -> {
                // lower it.
                this.skew = min(this.skew - this.skewNormalizeProperty, targetSkew)
            }
            // If the current skew is smaller than the target one,
            this.skew < targetSkew -> {
                // increase it.
                this.skew = max(this.skew + this.skewNormalizeProperty, targetSkew)
            }
        }
    }

    /**
     * The best time to remove this delay is on player update, as it is called every tick.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // Normalize the skew.
        this.normalizeSkew()
    }

}