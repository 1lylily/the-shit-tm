package tech.chloehook.module.impl.combat

import net.minecraft.network.play.server.S12PacketEntityVelocity
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PacketReceiveEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * Decreases the amount of knock-back you take.
 */
class Velocity(dependency: ModuleDependency) : Module(dependency,
    "Velocity",
    ModuleCategory.COMBAT,
    "Decreases the amount of velocity you take."
) {
    override var suffix: () -> String? = {
        val mode = modeProperty

        if (mode == "Percent") "${horizontalProperty}% ${verticalProperty}%"
        else mode
    }

    /**
     * The mode for this module.
     */
    private var modeProperty by mode("Mode", "Cancel", "The mode of the module.", "Cancel", "Percent")

    /**
     * The horizontal percentage for Percent mode.
     */
    private var horizontalProperty by int("Horizontal", 100, 0..100, "Horizontal velocity. (Percent)")

    /**
     * The vertical percentage for Percent mode.
     */
    private var verticalProperty by int("Vertical", 100, 0..100, "Vertical velocity. (Percent)")

    /**
     * The best time to remove this delay is on player update, as it is called every tick.
     */
    @Listen
    @Suppress("Unused")
    fun onPacketReceive(e: PacketReceiveEvent) {
        // If the packet is an uncancelled S12,
        if (!e.cancelled && e.packet is S12PacketEntityVelocity) {
            // get the packet and
            val packet: S12PacketEntityVelocity = e.packet
            // do things.
            when (modeProperty) {
                // If the mode is Cancel, cancel the event.
                "Cancel" -> e.cancelled = true
                // If the mode is Percent,
                "Percent" -> {
                    // adjust the horizontal
                    packet.motionX = (packet.motionX * (horizontalProperty / 100.0)).toInt()
                    packet.motionZ = (packet.motionZ * (horizontalProperty / 100.0)).toInt()
                    // and vertical velocity.
                    packet.motionY = (packet.motionY * (verticalProperty / 100.0)).toInt()
                }
            }
        }
    }

}