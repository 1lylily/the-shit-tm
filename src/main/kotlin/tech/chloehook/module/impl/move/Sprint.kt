package tech.chloehook.module.impl.move

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.TickEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * This module simulates the sprint bind being held.
 */
class Sprint(dependency: ModuleDependency) : Module(dependency,
    "Sprint",
    ModuleCategory.MOVE,
    "Automatically sprints."
) {

    /**
     * Run this on tick instead of update, as keybindings don't require you to be in a world.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onTick(e: TickEvent) {
        // Set the pressed state of the sprint keybinding to true.
        mc.gameSettings.keyBindSprint.pressed = true
    }

}