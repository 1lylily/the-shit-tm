package tech.chloehook.module.impl.move

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.PrePositionEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * When the player jumps by holding down their jump bind, there is a 10 tick delay between jumps. This module eliminates that.
 */
class NoJumpDelay(dependency: ModuleDependency) : Module(dependency,
    "No Jump Delay",
    ModuleCategory.MOVE,
    "Removes the delay between jumps."
) {

    /**
     * The best time to remove this delay is on player update, as it is called every tick.
     */
    @Listen
    @Suppress("Unused", "Unused_Parameter")
    fun onUpdate(e: PrePositionEvent) {
        // Set the jump ticks to 0, indicating that the delay is over.
        mc.thePlayer?.jumpTicks = 0
    }

}