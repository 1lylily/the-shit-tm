package tech.chloehook.module.impl.overlay

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import net.minecraft.util.EnumChatFormatting.*
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.Render2DEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency
import tech.chloehook.utils.render.Animation
import java.awt.Color

/**
 * This module reduces the delay between item uses.
 */
class HUD(dependency: ModuleDependency) : Module(dependency,
    "HUD",
    ModuleCategory.OVERLAY,
    "Renders the client's overlay."
) {

    /**
     * Shows the client name.
     */
    private var watermark by boolean("Watermark", true, "Shows the client name.")

    private var bzr by boolean("BZR", true, "BZR.")

    /**
     * Shows enabled modules.
     */
    private var arraylist by boolean("Arraylist", true, "Shows enabled modules.")

    private lateinit var arraylistEntries: List<ArraylistEntry>

    override fun onEnable() {
        this.arraylistEntries = this.getModules().map { ArraylistEntry(it.name, it.suffix, it::enabled) }
    }

    private val fr: FontRenderer by lazy { Minecraft.getMinecraft().fontRendererObj }

    private var lastFrame: Long = 0

    /**
     * Render it on frame.
     */
    @Listen
    @Suppress("Unused")
    fun onRenderOverlay(e: Render2DEvent) {
        val currentFrame = System.currentTimeMillis()
        val delta = currentFrame - this.lastFrame
        this.lastFrame = currentFrame
        if (currentFrame == delta) return

        if (watermark) {
            fr.drawStringWithShadow(if (bzr) "${BOLD}B${RESET}${WHITE}lue Zenith Reborn (Slay Era Edition)" else "${BOLD}C${RESET}${WHITE}hloeHook", 4.0F, 4.0F, 0xC3B1E1)
        }

        var height = 4F

        if (arraylist) {
            val render = arraylistEntries
                .onEach { it.animation.also { anim -> anim.reversed = !it.enabled() }.update(delta) }
                .filter { it.animation.scaleF != 0F }
                .map { "${BOLD}${it.name}${run {
                    val suffix = it.suffix()
                    if (suffix != null)
                        " ${RESET}${GRAY}${suffix}"
                    else ""
                }}" to it.animation.scaleF }
                .sortedByDescending { fr.getStringWidth(it.first) }

            render.forEach { (value, anim) ->
                fr.drawStringWithShadow(value, (e.sr.scaledWidth - (fr.getStringWidth(value) + 4) * anim), height, Color( 195, 177, 225, (anim * 255).toInt()).rgb)
                height += (11*anim)
            }
        }
    }

}

private data class ArraylistEntry(
    val name: String,
    val suffix: () -> String?,
    val enabled: () -> Boolean,
    val animation: Animation = Animation(0, 100, 1000)
)