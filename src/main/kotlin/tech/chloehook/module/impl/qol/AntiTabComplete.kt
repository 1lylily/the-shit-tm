package tech.chloehook.module.impl.qol

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.ChatCompleteEvent
import tech.chloehook.module.api.Module
import tech.chloehook.module.api.ModuleCategory
import tech.chloehook.module.api.ModuleDependency

/**
 * This module stops tab autocompletion in chat.
 */
class AntiTabComplete(dependency: ModuleDependency) : Module(dependency,
    "Anti Tab Complete",
    ModuleCategory.QOL,
    "Stops tab autocomplete in chat."
) {

    /**
     * Cancel the ChatComplete event.
     */
    @Listen
    @Suppress("Unused")
    fun onTabComplete(e: ChatCompleteEvent) {
        // Simply cancel the event.
        e.cancelled = true
    }

}