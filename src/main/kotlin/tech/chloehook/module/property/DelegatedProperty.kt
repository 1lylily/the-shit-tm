package tech.chloehook.module.property

import tech.chloehook.module.api.Module
import kotlin.reflect.KProperty

class DelegatedProperty<Type : Any>(
    private var value: Type,
    name: String,
    description: String,
    add: (String, DelegatedProperty<*>) -> Unit,
    private val validate: (Type, Type) -> Type = { _, new -> new }
) : Property<Type>(name, description) {

    init {
        add(internalName, this)
    }

    operator fun getValue(thisRef: Module, property: KProperty<*>): Type = this.value

    operator fun setValue(thisRef: Module, property: KProperty<*>, value: Type) { this.value = this.validate(this.value, value) }

    private fun setSelfChecked(value: Type) { this.value = this.validate(this.value, value) }

    override fun setSelf(value: Type) { this.value = value }

    override fun getSelf(): Type = this.value

    @Suppress("Unchecked_Cast")
    fun setSelfString(value: String) {
        setSelfChecked(when (this.value) {
            is Int -> {
                value.toIntOrNull()!!
            }
            is Double -> {
                value.toDoubleOrNull()!!
            }
            is Boolean -> {
                value.toBooleanStrictOrNull()!!
            }
            is String -> {
                value
            }
            else -> {
                return
            }
        } as Type)
    }

}