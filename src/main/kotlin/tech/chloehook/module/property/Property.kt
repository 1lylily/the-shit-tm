package tech.chloehook.module.property

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import tech.chloehook.config.api.ISerializationElement

abstract class Property<Type : Any>(
    val name: String,
    val description: String,
) : ISerializationElement {

    abstract fun setSelf(value: Type)
    abstract fun getSelf() : Type

    val internalName: String = name.lowercase().replace(" ", "-")

    override fun serialize(json: JsonObject) {
        json["properties"].asJsonObject.add(
            this.internalName,
            JsonObject().also {
                it.addValue(getSelf())
            }
        )
    }

    override fun deserialize(json: JsonObject) {
        json["properties"].asJsonObject.run {
            if(this.has(this@Property.internalName)) {
                this@Property.setSelf(this.readValue(this@Property.getSelf()))
            }
        }
    }

    private companion object {
        fun <T : Any> JsonObject.addValue(value: T) {
            when (value) {
                is String -> {
                    this.addProperty("value", value as String)
                }
                is Double -> {
                    this.addProperty("value", value as Double)
                }
                is Int -> {
                    this.addProperty("value", value as Int)
                }
                is Boolean -> {
                    this.addProperty("value", value as Boolean)
                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        fun <T : Any> JsonElement.readValue(value: T): T {
            return when (value) {
                is String -> {
                    this.asString as T
                }
                is Double -> {
                    this.asDouble as T
                }
                is Int -> {
                    this.asInt as T
                }
                is Boolean -> {
                    this.asBoolean as T
                }
                else -> {
                    throw IllegalArgumentException("Value of invalid type.")
                }
            }
        }
    }

}