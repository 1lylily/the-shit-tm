package tech.chloehook.module.api

import net.minecraft.client.Minecraft
import tech.chloehook.module.property.DelegatedProperty
import kotlin.math.max
import kotlin.math.min

abstract class Module(
    dependency: ModuleDependency,
    val name: String,
    val category: ModuleCategory,
    val description: String = "Placeholder.",
    var bind: Int = 0
) : ModuleDependency by dependency {

    var enabled: Boolean = false
        set(value) {
            if (value != field) {
                field = value
                if (field) {
                    this.onEnable()
                    println("[ChloeHook Module] onEnable @ ${this.javaClass.canonicalName}")
                    if (field) {
                        this.deps_subscribe(this)
                    }
                } else {
                    println("[ChloeHook Module] onDisable @ ${this.javaClass.canonicalName}")
                    this.deps_unsubscribe(this)
                    this.onDisable()
                }
            }
        }

    open fun onEnable() {}

    open fun onDisable() {}

    open var suffix: () -> String? = { null }

    protected fun getModules(): Collection<Module> = this.deps_getModules()

    protected val mc: Minecraft
        get() = Minecraft.getMinecraft()

    val properties: MutableMap<String, DelegatedProperty<*>> = HashMap()

    @Suppress("UNCHECKED_CAST")
    protected fun <T : Any> property(name: String): DelegatedProperty<T> =
        (properties[name] ?: throw NullPointerException("No such property.")) as DelegatedProperty<T>

    protected fun boolean(name: String, value: Boolean, description: String = "Placeholder."): DelegatedProperty<Boolean> =
        DelegatedProperty(value, name, description, properties::put)

    protected fun double(name: String, value: Double, range: ClosedFloatingPointRange<Double>, description: String = "Placeholder."): DelegatedProperty<Double> =
        DelegatedProperty(value, name, description, properties::put) { _, new -> range.clamp(new) }

    protected fun int(name: String, value: Int, range: IntRange, description: String = "Placeholder."): DelegatedProperty<Int> =
        DelegatedProperty(value, name, description, properties::put) { _, new -> range.clamp(new) }

    protected fun mode(name: String, value: String, description: String = "Placeholder.", vararg values: String): DelegatedProperty<String> =
        DelegatedProperty(value, name, description, properties::put) { old, new -> values.containsLoose(new) ?: old }

}

private fun ClosedFloatingPointRange<Double>.clamp(value: Double): Double = min(max(value, start), endInclusive)
private fun IntRange.clamp(value: Int): Int = min(max(value, start), endInclusive)
private fun Array<out String>.containsLoose(value: String): String? = this.find { it.equals(value, true) }