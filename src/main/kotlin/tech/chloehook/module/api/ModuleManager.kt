package tech.chloehook.module.api

import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.KeyPressEvent
import tech.chloehook.internal.ISubsystem
import tech.chloehook.module.impl.combat.*
import tech.chloehook.module.impl.move.*
import tech.chloehook.module.impl.player.*
import tech.chloehook.module.impl.qol.*
import tech.chloehook.module.impl.overlay.*
import kotlin.reflect.KClass

/**
 * Manages ChloeHook's modules
 */
class ModuleManager(
    private val subscribe: (Any) -> Unit,
    private val unsubscribe: (Any) -> Unit,
    registerSubsystem: (ISubsystem) -> Unit
) {

    private val dependency: ModuleDependency = object : ModuleDependency {
        override fun deps_getModules(): Collection<Module> = this@ModuleManager.getModules()
        override fun <T : Module> dep_getModule(kClass: KClass<T>): T {
            @Suppress("Unchecked_Cast")
            return (this@ModuleManager.getModule(kClass) ?: throw IllegalArgumentException("Module not found.")) as T
        }

        override fun deps_subscribe(subscriber: Any) = this@ModuleManager.subscribe(subscriber)
        override fun deps_unsubscribe(subscriber: Any) = this@ModuleManager.unsubscribe(subscriber)

    }

    /**
     * A map of KClass to Module.
     */
    private val modules: Map<KClass<out Module>, Module> = listOf(
        // Combat modules.
        NoHitDelay(dependency),
        Velocity(dependency),
        Aura(dependency),
        AutoPot(dependency),
        // Move modules.
        Sprint(dependency),
        NoJumpDelay(dependency),
        // Player modules.
        FastPlace(dependency),
        SpeedMine(dependency),
        AntiVoid(dependency),
        Scaffold(dependency),
        // QoL modules.
        AntiTabComplete(dependency),
        // Overlay modules.
        HUD(dependency)
    )
            // Associate them by class.
            .associateBy { it::class }

    private fun getModule(kClass: KClass<out Module>): Module? = modules[kClass]

    fun getModules(): Collection<Module> {
        return modules.values
    }

    /**
     * Initializes the module map and prepares to listen for keyboard inputs.
     */
    init {
        // Register the module manager subsystem.
        registerSubsystem(object : ISubsystem {
            override fun registerSelf(register: (Any) -> Unit) = register(this)

            /**
             * Toggles modules when their bind is pressed.
             */
            @Listen
            @Suppress("Unused")
            fun onKey(e: KeyPressEvent) {
                if (e.state) {
                    modules.values
                        .filter {
                            it.bind == e.code
                        }
                        .forEach {
                            it.enabled = !it.enabled
                        }
                }
            }
        })
    }

}