package tech.chloehook.module.api

import kotlin.reflect.KClass

interface ModuleDependency {

    fun <T : Module> dep_getModule(kClass: KClass<T>): T

    fun deps_getModules(): Collection<Module>

    fun deps_subscribe(subscriber: Any)

    fun deps_unsubscribe(subscriber: Any)

}