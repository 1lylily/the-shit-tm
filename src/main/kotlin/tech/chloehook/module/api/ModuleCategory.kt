package tech.chloehook.module.api

enum class ModuleCategory(
    val displayName: String
) {
    MOVE("Move"),
    VISUAL("Visual"),
    OVERLAY("Overlay"),
    COMBAT("Combat"),
    PLAYER("Player"),
    QOL("QoL")
}