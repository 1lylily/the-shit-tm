package tech.chloehook.command.api

public interface ICommand {

    public val aliases: Array<String>

    public val syntax: String

    public fun isValid(split: List<String>): Boolean

    public fun handle(split: List<String>)

}