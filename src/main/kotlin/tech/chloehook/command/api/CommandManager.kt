package tech.chloehook.command.api

import net.minecraft.util.EnumChatFormatting
import tech.chloehook.command.impl.ModuleCommand
import tech.chloehook.command.impl.TestCommand
import tech.chloehook.event.api.Listen
import tech.chloehook.event.impl.ChatSendEvent
import tech.chloehook.internal.ISubsystem
import tech.chloehook.module.api.Module
import tech.chloehook.utils.addChatMessage

/**
 * Manages the chat commands for ChloeHook.
 *
 * @param registerSubsystem The function to register a subsystem.
 * @param getModules The function to get all modules.
 * @property prefix The prefix for commands.
 */
class CommandManager(
    registerSubsystem: (ISubsystem) -> Unit,
    getModules: () -> Collection<Module>,
    private val prefix: String,
) {

    /**
     * The alias-to-command map for chat commands.
     */
    private val commands: Map<String, ICommand>

    /**
     * Initializes the command map and listener subsystem.
     */
    init {
        // Create a temporary command map.
        val commandMap: MutableMap<String, ICommand> = HashMap()
        // Iterate through registered commands.
        listOf<ICommand>(
            TestCommand()
        ).forEach { command ->
            // Iterate through each of its aliases.
            command.aliases.forEach {
                // Add the alias to the temporary map.
                commandMap[it.trim().lowercase()] = command
            }
        }
        // Iterate through registered modules.
        getModules().forEach {
            // Add a ModuleCommand for it to the command map.
            commandMap[it.name.replace(" ", "").lowercase()] = ModuleCommand(it)
        }
        // Instantiate the command map.
        this.commands = HashMap(commandMap)
        println("[ChloeHook Command] ${this.commands.size} command map entries.")
        // Register the command listener subsystem.
        registerSubsystem(object : ISubsystem {
            override fun registerSelf(register: (Any) -> Unit) = register(this)

            /**
             * Listens for client commands being executed.
             */
            @Listen
            @Suppress("UNUSED")
            fun onChatSend(e: ChatSendEvent) {
                // Trim the message for trailing spaces.
                val message = e.message.trim()
                println("[ChloeHook Command] Command: $message")
                // Check if the message is a valid command.
                if (!e.cancelled && message.length > 1 && message.startsWith(prefix)) {
                    // Cancel the event.
                    e.cancelled = true
                    println("[ChloeHook Command] Valid command.")
                    // Split the message by space.
                    val split = message.substring(1).split(" ")
                    // Check if the command is valid, else return.
                    val command = this@CommandManager.commands[split[0].lowercase()] ?: return
                    println("[ChloeHook Command] Non-null.")
                    // Check if the command syntax is valid.
                    if (command.isValid(split)) {
                        println("[ChloeHook Command] Handling.")
                        // Handle the command.
                        command.handle(split)
                    } else {
                        // If not, return a syntax error.
                        addChatMessage("${EnumChatFormatting.RED}${command.syntax}")
                    }
                }
            }
        })
    }

}