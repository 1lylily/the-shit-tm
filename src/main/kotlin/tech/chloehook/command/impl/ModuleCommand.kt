package tech.chloehook.command.impl

import tech.chloehook.command.api.ICommand
import tech.chloehook.module.api.Module
import tech.chloehook.utils.addChatMessage
import net.minecraft.util.EnumChatFormatting.*
import org.lwjgl.input.Keyboard
import java.lang.IllegalStateException

class ModuleCommand(private val module: Module) : ICommand {
    override val aliases: Array<String> = emptyArray()
    override val syntax: String = "<module name> [bind [key] | ibind [keycode] | info | <setting name> [new value]]"

    private val validFirstArg = arrayOf("bind", "ibind", "info", * module.properties.values.map { it.internalName }.toTypedArray())

    override fun isValid(split: List<String>): Boolean {
        if (split.size !in validArgsSize) return false

        if (split.size >= 2) {
            if (split[1] !in validFirstArg) return false

            if (split.size == 3) {
                if (split[1] == "ibind" && intRegex.matchEntire(split[2]) == null) return false

                if (split[1] == "info") return false
            }
        }

        return true
    }

    override fun handle(split: List<String>) {
        when (split.size) {
            1 -> {
                this.module.enabled = !this.module.enabled
                addChatMessage("${this.module.name} has been ${if (this.module.enabled) "${GREEN}enabled" else "${RED}disabled"}${RESET}.")
            }
            2 -> {
                when (split[1]) {
                    "bind", "ibind" -> {
                        this.module.bind = Keyboard.KEY_NONE
                        addChatMessage("${this.module.name} has been unbound.")
                    }
                    "info" -> {
                        addChatMessage("   ${BOLD}${this.module.name}")
                        addChatMessage("${BOLD}Enabled: ${RESET}${if (this.module.enabled) "${GREEN}Yes" else "${RED}No"}")
                        addChatMessage("${BOLD}Bind: ${RESET}${Keyboard.getKeyName(this.module.bind)}")
                        addChatMessage("${BOLD}Properties: ${RESET}${module.properties.values.map { it.internalName }.toTypedArray().joinToString(", ")}")
                    }
                    else -> {
                        val property = module.properties[split[1].lowercase()] ?: throw IllegalStateException("Impossible error.")
                        addChatMessage("${property.name} has value ${BOLD}${property.getSelf()}${RESET}.")
                    }
                }
            }
            3 -> {
                when (split[1]) {
                    "ibind" -> {
                        this.module.bind = split[2].toIntOrNull()!!
                        addChatMessage("${this.module.name} has been bound to ${BOLD}${Keyboard.getKeyName(this.module.bind)}${RESET}.")
                    }
                    "bind" -> {
                        this.module.bind = Keyboard.getKeyIndex(split[2].uppercase())
                        addChatMessage("${this.module.name} has been bound to ${BOLD}${Keyboard.getKeyName(this.module.bind)}${RESET}.")
                    }
                    else -> {
                        val property = module.properties[split[1].lowercase()] ?: throw IllegalStateException("Impossible error.")
                        try {
                            property.setSelfString(split[2])
                            addChatMessage("${property.name} has been set to ${BOLD}${property.getSelf()}${RESET}.")
                        } catch (_: Throwable) {
                            addChatMessage("Failed to set value.")
                        }
                    }
                }
            }
        }
    }

}

private val validArgsSize = 1..3
private val intRegex = "[0-9]+".toRegex()