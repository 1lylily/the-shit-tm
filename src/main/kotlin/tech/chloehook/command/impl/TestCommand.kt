package tech.chloehook.command.impl

import tech.chloehook.command.api.ICommand
import tech.chloehook.utils.addChatMessage

class TestCommand : ICommand {
    override val aliases: Array<String> = arrayOf("test")
    override val syntax: String = "test"
    override fun isValid(split: List<String>): Boolean = true

    override fun handle(split: List<String>) {
        addChatMessage("Test command.")
        addChatMessage("Args: ${split.joinToString(",", "`", "`")}")
    }

}