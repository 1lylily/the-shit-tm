package tech.chloehook.main

import tech.chloehook.command.api.CommandManager
import tech.chloehook.event.api.Listen
import tech.chloehook.event.api.PubSub
import tech.chloehook.event.impl.StartGameEvent
import tech.chloehook.hooks.api.HookManager
import tech.chloehook.internal.ISubsystem
import tech.chloehook.internal.SubsystemManager
import tech.chloehook.module.api.ModuleManager
import java.lang.instrument.Instrumentation

/**
 * The main class of ChloeHook.
 */
object ChloeHook {

    /**
     * Holds the instrumentation object of the agent for possible later use.
     */
    private lateinit var inst: Instrumentation

    /**
     * The client's event bus, RadBus.
     */
    @SuppressWarnings("WeakerAccess")
    lateinit var eventBus: PubSub

    /**
     * The client's subsystem manager.
     */
    private lateinit var subsystemManager: SubsystemManager

    /**
     * The client's module manager.
     */
    private lateinit var moduleManager: ModuleManager

    /**
     * The client's command manager.
     */
    private lateinit var commandManager: CommandManager

    /**
     * Pre-initializes ChloeHook.
     *
     * This function does 2 things:
     * 1. Prepare the client for initialization.
     * 2. Register the hook transformer.
     *
     * @param instrumentation The instrumentation object of the agent.
     * @param runtime Whether the injection is runtime or premain.
     */
    @JvmStatic
    @Suppress("Unused")
    fun preInit(
        instrumentation: Instrumentation,
        runtime: Boolean
    ) {
        // Set the instrumentation object.
        this.inst = instrumentation
        // Initialize the Event Bus.
        println("[ChloeHook] Initializing Event Bus...")
        this.eventBus = PubSub
        // Prepare the subsystem manager.
        this.subsystemManager = SubsystemManager(this.eventBus::subscribe)
        // Finish pre-initialization.
        println("[ChloeHook] Finished pre-initialization.")
        // Initialize the subsystem manager.
        this.subsystemManager.initialize()
        // Register the initialization subsystem.
        this.subsystemManager.register(object : ISubsystem {
            override fun registerSelf(register: (Any) -> Unit) {
                println("[ChloeHook Debug] Registering init subsystem.")
                register(this)
            }

            /**
             * Initializes the client.
             */
            @Listen
            @Suppress("Unused", "Unused_Parameter")
            fun onGameStart(e: StartGameEvent) {
                println("[ChloeHook] Initializing...")
                // Initialize the module system.
                this@ChloeHook.moduleManager = ModuleManager(
                    this@ChloeHook.eventBus::subscribe,
                    this@ChloeHook.eventBus::unsubscribe,
                    this@ChloeHook.subsystemManager::register
                )
                println("[ChloeHook] Initialized module manager.")
                // Initialize the command manager.
                this@ChloeHook.commandManager = CommandManager(
                    this@ChloeHook.subsystemManager::register,
                    this@ChloeHook.moduleManager::getModules,
                    "."
                )
                // Finish initialization.
                println("[ChloeHook] Finished initialization.")
            }
        })
        // Register the hook transformer.
        val hookManager = HookManager(inst, runtime)
        // Runtime injection.
        if (runtime) {
            println("[ChloeHook] Doing runtime initialization.")
            // Fire a StartGameEvent, as we won't get any of those.
            this.eventBus.publish(StartGameEvent())
        }
    }


}