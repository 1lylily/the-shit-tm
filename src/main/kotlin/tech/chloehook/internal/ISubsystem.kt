package tech.chloehook.internal

public interface ISubsystem {

    fun registerSelf(register: (Any) -> Unit)

}