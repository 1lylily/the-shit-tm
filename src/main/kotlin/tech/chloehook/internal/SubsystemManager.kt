package tech.chloehook.internal

/**
 * Manages internal client subsystems.
 */
internal class SubsystemManager(private val subscribe: (Any) -> Unit) {

    /**
     * Holds a list of available subsystems.
     */
    private val subsystems: MutableList<ISubsystem> = ArrayList()

    /**
     * Whether the subsystem manager has initialized yet.
     */
    private var initialized: Boolean = false

    /**
     * Registers a new subsystem.
     */
    fun register(subsystem: ISubsystem) {
        println("[ChloeHook SubSys] Registering subsystem.")
        // Add it to the list.
        this.subsystems.add(subsystem)
        // If the manager has already initialized,
        if (this.initialized) {
            // initialize the subsystem.
            println("[ChloeHook SubSys] Registering subsystem events.")
            subsystem.registerSelf(subscribe)
        }
    }

    /**
     * Initializes the subsystem manager.
     */
    fun initialize() {
        // If the manager has already initialized,
        if (this.initialized)
            // throw an error.
            throw IllegalStateException("Already initialized.")
        // Initialize the registered subsystems.
        subsystems.forEach { it.registerSelf(this.subscribe) }
        // Prevent initialization again.
        this.initialized = true
    }

}