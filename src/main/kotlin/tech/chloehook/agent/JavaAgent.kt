package tech.chloehook.agent

import java.lang.instrument.Instrumentation

@Suppress("Unused_Parameter")
fun premain(args: String?, inst: Instrumentation) {

    println("[ChloeHook] Premain.")

    inst.addTransformer(ClientLoadTransformer(inst))
}

@Suppress("Unused_Parameter")
fun agentmain(args: String?, inst: Instrumentation) {
    println("[ChloeHook] Agentmain, assuming runtime injection.")

    (inst.allLoadedClasses.find {
        try {
            return@find it?.classLoader?.toString()?.contains("GenesisClassLoader") ?: false
        } catch (_: Throwable) {}
        false
    }?.also {
        println("[ChloeHook] Found GenesisClassLoader.")
        println("[ChloeHook] Retransformation supported: ${inst.isRetransformClassesSupported}")
        }?.classLoader
        ?.loadClass("tech.chloehook.main.ChloeHook")
        ?.getDeclaredMethod("preInit", Instrumentation::class.java, Boolean::class.java)
        ?: throw NullPointerException("Could not find Genesis."))?.invoke(null, inst, true)
}