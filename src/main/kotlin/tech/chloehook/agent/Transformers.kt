package tech.chloehook.agent

import java.lang.instrument.ClassFileTransformer
import java.lang.instrument.Instrumentation
import java.security.ProtectionDomain

/**
 * Transformer that loads ChloeHook itself.
 *
 * This works by:
 * 1. Waiting for a Minecraft class to be loaded, as it will be through Genesis.
 * 2. Using Genesis to load the client.
 *
 * @property inst The instrumentation object of the agent.
 */
internal class ClientLoadTransformer(
    private val inst: Instrumentation
) : ClassFileTransformer {

    /**
     * Runs the transformation.
     */
    override fun transform(
        loader: ClassLoader?,
        className: String,
        classBeingRedefined: Class<*>?,
        protectionDomain: ProtectionDomain?,
        classfileBuffer: ByteArray
    ): ByteArray? {
        // If a minecraft class is being loaded.
        if(className.startsWith("net/minecraft/client/")) {
            // Remove the transformer, we only need this once.
            this.inst.removeTransformer(this)
            // Pre-initialize the client.
            loader?.loadClass("tech.chloehook.main.ChloeHook")
                ?.getDeclaredMethod("preInit", Instrumentation::class.java, Boolean::class.java)
                ?.invoke(null, inst, false)
        }
        // Return null, since we are not modifying anything.
        return null
    }
}