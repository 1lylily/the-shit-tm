package tech.chloehook.utils

import net.minecraft.client.Minecraft
import net.minecraft.util.ChatComponentText
import net.minecraft.util.EnumChatFormatting.*

/**
 * If thePlayer is not null, add the given message to the client-side chat.
 */
fun addChatMessage(message: String) {
    Minecraft.getMinecraft().thePlayer?.addChatMessage(ChatComponentText("${BOLD}CH> ${RESET}$message"))
}