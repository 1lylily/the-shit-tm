package tech.chloehook.utils

import net.minecraft.client.Minecraft
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.MathHelper
import net.minecraft.util.MovingObjectPosition
import net.minecraft.util.Vec3
import kotlin.math.*

typealias Rotations = Pair<Float, Float>

/**
 * Multiply with a degree to turn it into a radian.
 */
const val TO_RADS = 180F / PI.toFloat()

/**
 * Calculates the needed yaw and pitch to aim at the target.
 *
 * @param entity The target entity.
 * @return The yaw and pitch to aim at.
 * @author nevalackin (pasted from radium src)
 */
@Deprecated("Deprecated in favor of Hit Vecs.")
fun EntityPlayerSP.getRotations(entity: Entity): Rotations {
    val xDif: Double = entity.posX - this.posX
    val zDif: Double = entity.posZ - this.posZ

    val entityBB: AxisAlignedBB = entity.entityBoundingBox.expand(0.1, 0.1, 0.1)
    val playerEyePos = this.posY + this.eyeHeight
    val yDif: Double =
        if (playerEyePos > entityBB.maxY) entityBB.maxY - playerEyePos else  // Higher than max, aim at max
            if (playerEyePos < entityBB.minY) entityBB.minY - playerEyePos else  // Lower than min, aim at min
                0.0 // Else aim straight

    val fDist = MathHelper.sqrt_double(xDif * xDif + zDif * zDif).toDouble()

    return ((StrictMath.atan2(zDif, xDif) * TO_RADS).toFloat() - 90.0f to
        -(StrictMath.atan2(yDif, fDist) * TO_RADS).toFloat())
}

/**
 * Calculates the needed yaw and pitch to aim at the target.
 *
 * @return The yaw and pitch to aim at.
 * @author nevalackin (pasted from radium src)
 */
val Vec3.rotations: Rotations
    get() = (atan2(zCoord, xCoord) * TO_RADS).toFloat() - 90F to
            -(atan2(yCoord, sqrt(xCoord.pow(2) + zCoord.pow(2))) * TO_RADS).toFloat()

/**
 * Calculates whether the given rotation and source intercept with the given bounding box inside the given reach.
 *
 * @param source The source of the ray trace.
 * @param hitVec The [rotationsVector] of the rotation of the source.
 * @param reach The reach to search for.
 * @return A BLOCK MovingObjectPosition with a hitVec and no BlockPos if the ray trace hit, null otherwise.
 */
fun AxisAlignedBB.rayTrace(source: Vec3, hitVec: Vec3, reach: Double): MovingObjectPosition? =
    // Use calculateIntercept to ray trace.
    this.calculateIntercept(source, source.addVector(hitVec.xCoord * reach, hitVec.yCoord * reach, hitVec.zCoord * reach))


/**
 * Gets the closest point of a bounding box.
 *
 * @param source The source to search from.
 * @return A Vec3 of the closest point.
 */
fun AxisAlignedBB.getClosestPoint(source: Vec3): Vec3 {
    // Calculate the closest X point.
    val closestX =
        // If the source X is bigger than the bounding box max X,
        if (source.xCoord >= this.maxX)
            // use the bounding box max X.
            this.maxX
        // If the source X is smaller than the bounding box min X,
        else if (source.xCoord <= this.minX)
            // use the bounding box min X.
            this.minX
        // Otherwise use the source X.
        else this.minX + (source.xCoord - this.minX)
    // Calculate the closest Y point.
    val closestY =
        // If the source Y is bigger than the bounding box max Y,
        if (source.yCoord >= this.maxY)
            // use the bounding box max Y.
            this.maxY
        // If the source Y is smaller than the bounding box min Y,
        else if (source.yCoord <= this.minY)
            // use the bounding box min Y.
            this.minY
        // Otherwise use the source Y.
        else this.minY + (source.yCoord - this.minY)
    // Calculate the closest Z point.
    val closestZ =
        // If the source X is bigger than the bounding box max X,
        if (source.zCoord >= this.maxZ)
            // use the bounding box max X.
            this.maxZ
        // If the source X is smaller than the bounding box min X,
        else if (source.zCoord <= this.minZ)
            // use the bounding box min X.
            this.minZ
        // Otherwise use the source X.
        else this.minZ + (source.zCoord - this.minZ)
    // Return the Vec3 of these positions.
    return Vec3(closestX, closestY, closestZ)
}

/**
 * Get vector for rotation.
 *
 * TODO: Deobfuscate minecraft fields.
 *
 * @see Entity.getVectorForRotation
 */
val Rotations.rotationsVector: Vec3 get() {
    val var3 = cos(-second * TO_RADS - PI)
    val var4 = sin(-second * TO_RADS - PI)
    val var5 = -cos(-first * TO_RADS)
    val var6 = sin(-first * TO_RADS)
    return Vec3((var4 * var5), var6.toDouble(), (var3 * var5))
}

/**
 * Calculates the black magic fuckery.
 *
 * TODO: Document this when I figure out how it works.
 */
private fun getMouseGCD(): Double {
    val sens = Minecraft.getMinecraft().gameSettings.mouseSensitivity * 0.6f + 0.2f
    val pow = sens * sens * sens * 8.0f
    return pow.toDouble() * 0.15
}

/**
 * Gets the closest point from the source to the entity's hittable bounding box.
 *
 * @param source The hit source position.
 */
fun EntityLivingBase.closestPoint(source: Vec3): Vec3 =
    // Get the closest point to the entity's hittable bounding box.
    this.entityBoundingBox.expand(
        this.collisionBorderSize.toDouble(),
        this.collisionBorderSize.toDouble(),
        this.collisionBorderSize.toDouble()
    ).getClosestPoint(source)

/**
 * Get the hit distance from the source to the nearest point of the entity.
 *
 * @param source The hit source position.
 * @param closestPoint The closest point, if not specified will be calculated with [EntityLivingBase.closestPoint]
 */
fun EntityLivingBase.hitDistanceFrom(source: Vec3, closestPoint: Vec3 = this.closestPoint(source)): Double =
    // Get the closest point to the entity's hittable bounding box.
    closestPoint
        // Get its delta to the source.
        .absoluteDelta(source)
        // Convert it to a square distance.
        .sqDist()