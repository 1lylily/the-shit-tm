package tech.chloehook.utils

import net.minecraft.client.Minecraft
import net.minecraft.network.Packet

/**
 * Sends the given packet.
 *
 * @param packet The packet to send.
 */
fun sendPacket(packet: Packet<*>) {
    Minecraft.getMinecraft().netHandler?.addToSendQueue(packet)
}