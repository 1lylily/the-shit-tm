package tech.chloehook.utils

import net.minecraft.util.BlockPos
import net.minecraft.util.Vec3
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

fun clampZeroToOne(value: Float): Float = min(max(value, 0F), 1F)

fun clampZeroToOne(value: Double): Double = min(max(value, 0.0), 1.0)

fun clamp(value: Double, min: Double, max: Double): Double = min(max(value, min), max)

fun Float.floor(): Int = (this - (this % 1)).toInt()

fun Double.floor(): Int = (this - (this % 1)).toInt()

fun Float.ceil(): Int = (this - (this % 1) + 1).toInt()

fun BlockPos.distanceSq(vec: Vec3): Double = this.distanceSq(vec.xCoord, vec.yCoord, vec.zCoord)

fun Vec3.absoluteDelta(vec: Vec3): Vec3 = Vec3(abs(this.xCoord - vec.xCoord), abs(this.yCoord - vec.yCoord), abs(this.zCoord - vec.zCoord))

fun Vec3.delta(vec: Vec3): Vec3 = Vec3(this.xCoord - vec.xCoord, this.yCoord - vec.yCoord, this.zCoord - vec.zCoord)

fun Vec3.sqDist(): Double = sqrt(this.xCoord * this.xCoord + this.yCoord * this.yCoord + this.zCoord * this.zCoord)

val random: Random = Random(727)

val gaussianRandom: Double get() = random.nextGaussian()