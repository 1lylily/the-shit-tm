package tech.chloehook.utils.render

import tech.chloehook.utils.clampZeroToOne
import kotlin.math.max
import kotlin.math.min

/**
 * A basic linear animation with no easing.
 *
 * @property min The lower bound for the animation value.
 * @property max The upper bound for the animation value.
 * @property speed The amount to step every second.
 * @property value The current value for the animation.
 * @property reversed Whether to go backwards in the animation.
 */
class Animation(
    val min: Int,
    val max: Int,
    var speed: Int
) {
    private var realValue: Double = 0.0
    var reversed: Boolean = false

    val value: Int get() = realValue.toInt()
    val scaleF: Float get() = clampZeroToOne((realValue/max).toFloat())

    /**
     * Update the animation, run this once per frame.
     *
     * @param delta The milliseconds since the last frame.
     */
    fun update(delta: Long) {
        realValue += rev(delta / 1000.0) * speed
        realValue = clamp(realValue)
    }

    private fun rev(value: Double): Double = if (reversed) -value else value

    private fun clamp(value: Double): Double = min(max(value, this.min.toDouble()), this.max.toDouble())

}