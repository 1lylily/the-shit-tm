package tech.chloehook.utils

import net.minecraft.entity.Entity
import net.minecraft.util.BlockPos
import net.minecraft.util.EnumFacing
import net.minecraft.util.Vec3
import net.minecraft.util.Vec3i

fun <K, V> MutableMap<K, V>.putAndGet(key: K, value: V): V {
    this[key] = value
    return value
}

operator fun Vec3.component1(): Double = this.xCoord
operator fun Vec3.component2(): Double = this.yCoord
operator fun Vec3.component3(): Double = this.zCoord

val Vec3.blockPos: BlockPos get() = BlockPos(this.xCoord.toInt(), this.yCoord.toInt(), this.zCoord.toInt())

val Entity.hitOrigin: Vec3 get() = this.positionVector.addVector(0.0, this.eyeHeight.toDouble(), 0.0)

val Entity.nextTickHitOrigin: Vec3 get() = this.positionVector.addVector(this.motionX, this.motionY + this.eyeHeight.toDouble(), this.posZ)

val Vec3i.sum: Int get() = this.x + this.y + this.z

val EnumFacing.offset: Double get() = (this.directionVec.sum + 1.0) / 2

val EnumFacing.offsetX: Double get() = (this.directionVec.x + 1.0) / 2
val EnumFacing.offsetZ: Double get() = (this.directionVec.z + 1.0) / 2