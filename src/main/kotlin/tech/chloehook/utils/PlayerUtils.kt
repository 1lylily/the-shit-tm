package tech.chloehook.utils

import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.potion.Potion
import net.minecraft.util.BlockPos

/**
 * Checks if the player is above the void, with a default depth of 30 blocks.
 *
 * @param depth The depth to check.
 */
fun EntityPlayerSP.isAboveVoid(depth: Int = 30, ignoreGround: Boolean = false): Boolean {
    // If we are on the ground and ignoreGround is false, return false.
    if (!ignoreGround && this.onGround) return false
    // Get the current positions of the player.
    var (x, y, z) = this.positionVector
    // Iterate through the N blocks under the player, N being the depth.
    val targetY = y - depth
    while (y >= targetY) {
        // Check the block material at the target position.
        if (this.worldObj.getBlockState(BlockPos(x.toInt(), y.toInt(), z.toInt())).block.isCollidable)
            // Return false, found a solid block.
            return false
        // Decrement the Y
        y--
    }
    // Return true, no solid blocks found below.
    return true
}

/**
 * Move the player up N blocks and update.
 *
 * @param blocks The amount to move.
 */
fun EntityPlayerSP.up(blocks: Int) {
    // Move up, nothing complicated.
    this.setPositionAndUpdate(this.posX, this.posY + blocks, this.posZ)
}

val EntityPlayerSP.moving: Boolean
    get() = this.moveForward != 0F || this.moveStrafing != 0F

val EntityPlayerSP.hasSpeedEffect: Boolean
    get() = this.isPotionActive(Potion.moveSpeed)