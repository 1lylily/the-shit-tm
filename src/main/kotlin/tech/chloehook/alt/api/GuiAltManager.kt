package tech.chloehook.alt.api

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiMainMenu
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.ScaledResolution
import net.minecraft.util.Session
import java.lang.reflect.Field

class GuiAltManager(val screen: GuiScreen) : GuiScreen() {

    override fun drawScreen(p0: Int, p1: Int, p2: Float) {
        this.drawDefaultBackground()
        fontRendererObj.drawStringWithShadow("Username: ${mc.session.username}", 4F, 4F, -1)
        super.drawScreen(p0, p1, p2)
    }

    override fun actionPerformed(p0: GuiButton?) {
        when (p0?.id) {
            0 -> mc.displayGuiScreen(screen)
            1 -> {
                var name = "ThisIsAnInvalidNameLol"
                while (name.length > 14) {
                    name = "${adjectives.random()}${nouns.random()}"
                }
                while (name.length < 16) {
                    name += numbers.random()
                }
                sessionField.set(mc, Session(name, "", "", "mojang"))
            }
        }
        super.actionPerformed(p0)
    }

    override fun initGui() {
        val sr = ScaledResolution(mc)
        this.buttonList.add(GuiButton(0, sr.scaledWidth/2-100, sr.scaledHeight/2+100, "Cancel"))
        this.buttonList.add(GuiButton(1, sr.scaledWidth/2-100, sr.scaledHeight/2+70, "Random"))
        super.initGui()
    }

}

private val sessionField: Field = Minecraft::class.java.getField("session")
private val adjectives: List<String> = listOf("superficial", "odd", "overconfident", "ragged", "next", "certain", "understood", "level", "alert", "envious")
private val nouns: List<String> = listOf("writer", "wood", "ratio", "shirt", "lake", "article", "desk", "mixture", "meat", "extent")
private val numbers: IntRange = 0..9

fun open(screen: GuiScreen) {
    Minecraft.getMinecraft().displayGuiScreen(GuiAltManager(screen))
}